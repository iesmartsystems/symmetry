//ROOM SETUP
Room: RoomName-
Room: PhoneNumber-
Room: NoTouchPowerOff-
Room: RoutingMode-
//DISPLAY(s) SETUP
//Display 1
Display 1: Name-Display
Display 1: ControlDeviceType-4
Display 1: ControlID-1
Display 1: ControlProtocol-
Display 1: SourceInputs-1,1,1,1,1,1,1,1,1
Display 1: PowerOnTime-5
Display 1: PowerOffTime-5
Display 1: OutputVideo-1
Display 1: ScreenUp-
Display 1: ScreenDown-
Display 1: ControlBaud-
Display 1: ControlParity-
Display 1: ControlIP-
Display 1: ControlIPPort-
Display 1: ControlLogin-
Display 1: ControlPassword-
//SWITCHER(s) SETUP
//Switcher 1
Switcher 1: ControlBaud-
Switcher 1: ControlParity-
Switcher 1: ControlIP-
Switcher 1: ControlIPPort-
Switcher 1: ControlLogin-
Switcher 1: ControlPassword-
//AUDIO(s) SETUP
//Audio 1
Audio 1: ObejctID-
Audio 1: ControlBaud-
Audio 1: ControlParity-
Audio 1: ControlIP-10.175.165.137
Audio 1: ControlIPPort-1710
Audio 1: ControlLogin-
Audio 1: ControlPassword-
Audio 1: InstanceTags 1-M2_LEVEL-1-SVOL
Audio 1: InstanceTags 2-
Audio 1: InstanceTags 3-
Audio 1: InstanceTags 4-
Audio 1: InstanceTags 5-
Audio 1: InstanceTags 6-
Audio 1: InstanceTags 7-
Audio 1: InstanceTags 8-
Audio 1: InstanceTags 9-
Audio 1: InstanceTags 10-
//CODEC(s) SETUP
//Codec 1
Codec 1: SourceName-
Codec 1: ContentName-
Codec 1: InputVideo-
Codec 1: IconType-
Codec 1: ControlBaud-
Codec 1: ControlParity-
Codec 1: ControlIP-
Codec 1: ControlIPPort-
Codec 1: ControlLogin-
Codec 1: ControlPassword-
//SOURCE(s) SETUP
//Source 1
Source 1: Name-
Source 1: InputVideo-
Source 1: IconType-
Source 1: ControlBaud-
Source 1: ControlParity-
Source 1: ControlIP-
Source 1: ControlIPPort-
Source 1: ControlLogin-
Source 1: ControlPassword-
//ENVIRONMENT(s) SETUP
//ENV 1
ENV 1: Name-
ENV 1: ControlBaud-
ENV 1: ControlParity-
ENV 1: ControlIP-
ENV 1: ControlIPPort-
ENV 1: ControlLogin-
ENV 1: ControlPassword-
//MISC(s) SETUP
//Strings & Analogs
//Misc 1 - STB Preset #1 - Channel #, Channel Logo
Misc 1: Strings-1029
Misc 1: Analogs-12
//Misc 2 - STB Preset #2 - Channel #, Channel Logo
Misc 2: Strings-1027
Misc 2: Analogs-33
//Misc 3 - STB Preset #3 - Channel #, Channel Logo
Misc 3: Strings-1031
Misc 3: Analogs-48
//Misc 4/5 - DSP Modules - Camera, Conference
Misc 4: Strings-M2_CAM
Misc 5: Strings-M2_CONF