//ROOM SETUP
Room: RoutingMode-2
//DISPLAY(S)_SETUP
//Displays
Display 1: Name-13.154D
Display 2: Name-13.654B
Display 3: Name-13.308C
Display 4: Name-13.402F
Display 5: Name-13th Break Room (L)
Display 6: Name-13th Break Room (R)
Display 7: Name-14th Reception
Display 8: Name-14th Break Room (L)
Display 9: Name-14th Break Room (R)
Display 10: Name-14.154D
Display 11: Name-14.302E
Display 12: Name-14.306E
//Display 13: Name-14.310J
//Display 14: Name-14.312H
//Display 15: Name-14.402B
//Display 16: Name-14.402D
//Display 17: Name-14.404H
//Display 18: Name-14.654B
//SOURCE(S)_SETUP
//Source 1
Source 1: Name-Golf - 635
Source 2: Name-CNBC - 645
Source 3: Name-Fox News - 638
Source 4: Name-CNN - 625
Source 5: Name-NBC Sports - 649
Source 6: Name-Weather Ch. - 627
Source 7: Name-Fox Business - 677
Source 8: Name-ESPN - 633
Source 1: IconType-3
Source 2: IconType-3
Source 3: IconType-3
Source 4: IconType-3
Source 5: IconType-3
Source 6: IconType-3
Source 7: IconType-3
Source 8: IconType-3
