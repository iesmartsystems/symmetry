/*
Dealer Name: i.e.SmartSytems
System Name:
System Number:
Programmer: Jesus Barrera
Comments: */
/*****************************************************************************************************************************
    i.e.SmartSystems LLC CONFIDENTIAL
    Copyright (c) 2011-2016 i.e.SmartSystems LLC, All Rights Reserved.
    NOTICE:  All information contained herein is, and remains the property of i.e.SmartSystems. The intellectual and 
    technical concepts contained herein are proprietary to i.e.SmartSystems and may be covered by U.S. and Foreign Patents, 
    patents in process, and are protected by trade secret or copyright law. Dissemination of this information or 
    reproduction of this material is strictly forbidden unless prior written permission is obtained from i.e.SmartSystems.  
    Access to the source code contained herein is hereby forbidden to anyone except current i.e.SmartSystems employees, 
    managers or contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
    The copyright notice above does not evidence any actual or intended publication or disclosure of this source code, 
    which includes information that is confidential and/or proprietary, and is a trade secret, of i.e.SmartSystems.   
    ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS SOURCE 
    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF i.e.SmartSystems IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE 
    LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY 
    OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  
    MAY DESCRIBE, IN WHOLE OR IN PART.
*****************************************************************************************************************************/

/*******************************************************************************************
  Compiler Directives
*******************************************************************************************/
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_DYNAMIC
#ENABLE_TRACE
#DEFINE_CONSTANT NUM_PRESETS 8
/*******************************************************************************************
  Include Libraries
*******************************************************************************************/
/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
*******************************************************************************************/
DIGITAL_INPUT Initialize, Poll, _SKIP_, Tilt_Up, Tilt_Down, Pan_Left, Pan_Right, Zoom_In, Zoom_Out, _SKIP_, Privacy_On, _SKIP_;
DIGITAL_INPUT Load_Preset[NUM_PRESETS], _SKIP_, Save_Preset[NUM_PRESETS], _SKIP_;
STRING_INPUT Control_Name[64], _SKIP_, DSP_RX[65534];
DIGITAL_OUTPUT CamPrivacyStatus, _SKIP_;
STRING_OUTPUT DSP_TX;
/*******************************************************************************************
  SOCKETS
*******************************************************************************************/
/*******************************************************************************************
  Parameters
*******************************************************************************************/
/*******************************************************************************************
  Parameter Properties
*******************************************************************************************/
/*******************************************************************************************
  Structure Definitions
*******************************************************************************************/
STRUCTURE sPTZCamera
{
	INTEGER StatusPrivacy;
	STRING RXQueue[65534];
	STRING ControlName[255];
	STRING Preset[32];
	STRING PresetCords[NUM_PRESETS][128];
	STRING TiltUp[255];
	STRING TiltDown[255];
	STRING PanLeft[255];
	STRING PanRight[255];
	STRING ZoomIn[255];
	STRING ZoomOut[255];
	STRING Privacy[255];
	STRING CompSet[255];
	STRING CompGet[255];
	STRING CompGet2[255];
	STRING GetPoll[255];
	STRING GetPTZ[255];
	STRING CurrentCords[64];
	STRING MTX[32];
	STRING ETX[16];
};
sPTZCamera Cam;
/*******************************************************************************************
  Global Variables
*******************************************************************************************/
INTEGER gvPreset;
/*******************************************************************************************
  Functions
*******************************************************************************************/
FUNCTION SetCommandValue( STRING InString, INTEGER InValue )
{
	STRING lvString[512];
	lvString = Cam.CompSet + InString + Cam.MTX + ITOA( InValue ) + Cam.ETX;
	DSP_TX = lvString;
}
FUNCTION SetCommandString( STRING InString, STRING InValue )
{
	STRING lvString[512];
	lvString = Cam.CompSet + InString + Cam.MTX + "\x22" + InValue + "\x22" + Cam.ETX;
	DSP_TX = lvString;
}
FUNCTION GetPTZ()
{
	STRING lvString[512];
	lvString = Cam.CompGet + Cam.Preset + Cam.CompGet2 + Cam.ETX;
	DSP_TX = lvString;
}
FUNCTION PollPrivacy()
{
	STRING lvString[512];
	lvString = Cam.CompGet + Cam.Privacy + Cam.CompGet2 + Cam.ETX;
	DSP_TX = lvString;
}
FUNCTION SavePreset( INTEGER lvIndex )
{
	GetPTZ();
	WAIT( 50 )
		Cam.PresetCords[gvPreset] = Cam.CurrentCords;
}
//{"jsonrpc":"2.0","result":{"Name":"CAM_155","Controls":[{"Name":"ptz.preset","String":"0 0 0","Value":0.0,"Position":0.0,"Indeterminate":false}]},"id":1234}
FUNCTION ParseFeedback()
{
	INTEGER lvIndex, lvNeg;
	SIGNED_INTEGER lvVol;
	STRING lvRemove[255], lvRX[65534], lvTrash[127], lvString[8191], lvCrdStr[32], lvChar[1];
	WHILE( FIND( "\x00", Cam.RXQueue ) )			//Incoming buffer still has info
	{  
		lvRX = REMOVE( "\x00", Cam.RXQueue );
		lvRX = REMOVEBYLENGTH( LEN( lvRX ) - 1, lvRX );
		IF( FIND( "\x22result\x22", lvRX ) )
		{
        	IF( FIND( Cam.Preset, lvRX ) )
			{
				lvRemove = Cam.Preset;
				lvTrash = REMOVE( lvRemove, lvRX );
				lvTrash = REMOVE( "\x22String\x22:\x22", lvRX );
				lvString = REMOVE( "\x22", lvRX );
				lvString = REMOVEBYLENGTH( LEN( lvString ) - 1, lvString );
				Cam.CurrentCords = lvString;
			}
        	IF( FIND( Cam.Privacy, lvRX ) )
			{
				lvRemove = Cam.Privacy;
				lvTrash = REMOVE( lvRemove, lvRX );
				lvTrash = REMOVE( "\x22Value\x22:", lvRX );
				lvString = REMOVE( ".", lvRX );
				lvString = REMOVEBYLENGTH( LEN( lvString ) - 1, lvString );
				CamPrivacyStatus = ATOI( lvString);
			}
		}
	} 
}
FUNCTION UpdateCommands()
{
	Cam.CompSet	= "{ \x22jsonrpc\x22: \x222.0\x22, \x22id\x22: 1234, \x22method\x22: \x22Component.Set\x22, \x22params\x22: { \x22Name\x22: \x22" + Cam.ControlName + "\x22, \x22Controls\x22: [ { \x22Name\x22: \x22";
	Cam.CompGet	= "{ \x22jsonrpc\x22: \x222.0\x22, \x22id\x22: 1234, \x22method\x22: \x22Component.Get\x22, \x22params\x22: { \x22Name\x22: \x22" + Cam.ControlName + "\x22, \x22Controls\x22: [ { \x22Name\x22: \x22";
	Cam.GetPoll	= "{ \x22jsonrpc\x22: \x222.0\x22, \x22id\x22: 1234, \x22method\x22: \x22Control.Get\x22, \x22params\x22: [\x22" + Cam.ControlName + "\x22]}\x00";
}
/*******************************************************************************************
  Event Handlers
*******************************************************************************************/
PUSH Poll
{
	DSP_TX = Cam.GetPoll;
}
CHANGE DSP_RX
{
	Cam.RXQueue = Cam.RXQueue + DSP_RX;
	ParseFeedback();	
}
CHANGE Control_Name
{
	Cam.ControlName = Control_Name;
	UpdateCommands();
}
CHANGE Tilt_Up
{
	STRING lvString[255];
	IF ( !Cam.StatusPrivacy )
	{	
		IF( Tilt_Up )
			SetCommandValue( Cam.TiltUp, 1 );
		ELSE
			SetCommandValue( Cam.TiltUp, 0 );
	}
}
CHANGE Tilt_Down
{
	STRING lvString[255];
	IF ( !Cam.StatusPrivacy )
	{	
		IF( Tilt_Down )
			SetCommandValue( Cam.TiltDown, 1 );
		ELSE
			SetCommandValue( Cam.TiltDown, 0 );
	}
}
CHANGE Pan_Left
{
	STRING lvString[255];
	IF ( !Cam.StatusPrivacy )
	{	
		IF( Pan_Left )
			SetCommandValue( Cam.PanLeft, 1 );
		ELSE
			SetCommandValue( Cam.PanLeft, 0 );
	}
}
CHANGE Pan_Right
{
	STRING lvString[255];
	IF ( !Cam.StatusPrivacy )
	{	
		IF( Pan_Right )
			SetCommandValue( Cam.PanRight, 1 );
		ELSE
			SetCommandValue( Cam.PanRight, 0 );
	}
}
CHANGE Zoom_In
{
	STRING lvString[255];
	IF ( !Cam.StatusPrivacy )
	{	
		IF( Zoom_In )
			SetCommandValue( Cam.ZoomIn, 1 );
		ELSE
			SetCommandValue( Cam.ZoomIn, 0 );
	}
}
CHANGE Zoom_Out
{
	STRING lvString[255];
	IF ( !Cam.StatusPrivacy )
	{	
		IF( Zoom_Out )
			SetCommandValue( Cam.ZoomOut, 1 );
		ELSE
			SetCommandValue( Cam.ZoomOut, 0 );
	}
}
CHANGE Privacy_On
{
	STRING lvString[255];
	IF( Privacy_On  )
		SetCommandValue( Cam.Privacy, 1 );
	ELSE
		SetCommandValue( Cam.Privacy, 0 );
	WAIT( 50 )
		PollPrivacy();
}
PUSH Load_Preset
{
	STRING lvString[255];
	INTEGER lvIndex;
	lvIndex = GETLASTMODIFIEDARRAYINDEX();
	gvPreset = lvIndex;
	SetCommandString( Cam.Preset, Cam.PresetCords[gvPreset] );
}
PUSH Save_Preset
{
	INTEGER lvIndex;
	lvIndex = GETLASTMODIFIEDARRAYINDEX();
	gvPreset = lvIndex;
	SavePreset( gvPreset );
}

/*******************************************************************************************
  Main()
*******************************************************************************************/
FUNCTION Main()
{
	INTEGER lvCounter;
	Cam.CompSet			= "{ \x22jsonrpc\x22: \x222.0\x22, \x22id\x22: 1234, \x22method\x22: \x22Component.Set\x22, \x22params\x22: { \x22Name\x22: \x22" + Cam.ControlName + "\x22, \x22Controls\x22: [ { \x22Name\x22: \x22";
	Cam.CompGet			= "{ \x22jsonrpc\x22: \x222.0\x22, \x22id\x22: 1234, \x22method\x22: \x22Component.Get\x22, \x22params\x22: { \x22Name\x22: \x22" + Cam.ControlName + "\x22, \x22Controls\x22: [ { \x22Name\x22: \x22";
	Cam.CompGet2		= "\x22";
	Cam.GetPoll			= "{ \x22jsonrpc\x22: \x222.0\x22, \x22id\x22: 1234, \x22method\x22: \x22Control.Get\x22, \x22params\x22: [\x22" + Cam.ControlName + "\x22]}\x00";
	Cam.MTX				= "\x22, \x22Value\x22: ";
	Cam.ETX			 	= " }]}}\x00";
	Cam.Preset		    = "ptz.preset";
	Cam.TiltUp		    = "tilt.up";
	Cam.TiltDown	    = "tilt.down";
	Cam.PanLeft		    = "pan.left";
	Cam.PanRight	    = "pan.right";
	Cam.ZoomIn		    = "zoom.in";
	Cam.ZoomOut		    = "zoom.out";
	Cam.Privacy			= "toggle.privacy";
	FOR( lvCounter = 1 TO NUM_PRESETS )
	{
		Cam.PresetCords[lvCounter] = "0 0 0";
	}
}
