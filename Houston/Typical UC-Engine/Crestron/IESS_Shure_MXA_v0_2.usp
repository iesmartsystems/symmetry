/*
Dealer Name: i.e.SmartSytems
System Name: IESS Shure MXA v0.2
System Number:
Programmer: Matthew Laletas
Comments: IESS Shure MXA v0.2
*/
/*****************************************************************************************************************************
    i.e.SmartSystems LLC CONFIDENTIAL
    Copyright (c) 2011-2016 i.e.SmartSystems LLC, All Rights Reserved.
    NOTICE:  All information contained herein is, and remains the property of i.e.SmartSystems. The intellectual and 
    technical concepts contained herein are proprietary to i.e.SmartSystems and may be covered by U.S. and Foreign Patents, 
    patents in process, and are protected by trade secret or copyright law. Dissemination of this information or 
    reproduction of this material is strictly forbidden unless prior written permission is obtained from i.e.SmartSystems.  
    Access to the source code contained herein is hereby forbidden to anyone except current i.e.SmartSystems employees, 
    managers or contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
    The copyright notice above does not evidence any actual or intended publication or disclosure of this source code, 
    which includes information that is confidential and/or proprietary, and is a trade secret, of i.e.SmartSystems.   
    ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS SOURCE 
    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF i.e.SmartSystems IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE 
    LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY 
    OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  
    MAY DESCRIBE, IN WHOLE OR IN PART.
*****************************************************************************************************************************/
/*******************************************************************************************
  Compiler Directives
*******************************************************************************************/
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_DYNAMIC
#ENABLE_TRACE
#HELP "IESS - Shure MXA v0.2"
#HELP ""
#HELP "INPUTS:"
#HELP "Working on it"
#HELP ""
#HELP "OUTPUTS:"
#HELP "Working on it"
/*******************************************************************************************
  Include Libraries
*******************************************************************************************/
/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
*******************************************************************************************/
DIGITAL_INPUT Connect, Poll, Debug, _SKIP_, LED_On, LED_Off, _SKIP_;
ANALOG_INPUT IP_Port;
STRING_INPUT IP_Address[31], _SKIP_, ManualCMD[255];
DIGITAL_OUTPUT Connect_FB, _SKIP_, LED_On_FB, LED_Off_FB, Mute_Button_On_FB, Mute_Button_Off_FB, _SKIP_;
ANALOG_OUTPUT Connect_Status_FB;
/*******************************************************************************************
  SOCKETS
*******************************************************************************************/
TCP_CLIENT AudioClient[1023];
/*******************************************************************************************
  Parameters
*******************************************************************************************/
/*******************************************************************************************
  Parameter Properties
*******************************************************************************************/
/*******************************************************************************************
  Structure Definitions
*******************************************************************************************/
STRUCTURE sAudio
{
	INTEGER StatusConnectRequest;
	INTEGER StatusConnected;
	INTEGER Reconnecting;
	STRING IPAddress[31];
	INTEGER IPPort;
	INTEGER MuteRequest;
	INTEGER StatusLED;
	STRING CommandLEDOn[31];
	STRING CommandLEDOff[31];
	STRING CommandPollLED[31];
	STRING CommandPollButtonState[31];
	STRING RXQueue[255];
};
sAudio AudioDevice;
/*******************************************************************************************
  Global Variables
*******************************************************************************************/
/*******************************************************************************************
  Functions
*******************************************************************************************/
FUNCTION SetDebug( STRING lvString, INTEGER lvType )
{
	IF( Debug )
	{
		IF( lvType = 1) //RX
			TRACE( "Shure RX: %s", lvString );
		ELSE			//TX
			TRACE( "Shure TX: %s", lvString );
	}
}
FUNCTION SetTXQueue( STRING lvString )
{
	STRING lvTemp[1023], lvTrash[31];
	IF( LEN( lvString ) > 3 )
	{
		lvTemp = "< " + lvString + " >";
		SOCKETSEND( AudioClient, lvTemp );
		SetDebug( lvTemp, 0 );
	}
}
FUNCTION ConnectDisconnect( INTEGER lvConnect )
{
	SIGNED_INTEGER lvStatus;
	IF( !lvConnect )
	{
		lvStatus = SOCKETDISCONNECTCLIENT( AudioClient );
	}
	ELSE IF( lvConnect )
	{
		lvStatus = SOCKETCONNECTCLIENT( AudioClient, AudioDevice.IPAddress, AudioDevice.IPPort, 0 );
	}
}			
/*******************************************************************************************
  Event Handlers
*******************************************************************************************/
//Connected
SOCKETCONNECT AudioClient
{
	AudioDevice.StatusConnected = 1;
	AudioDevice.Reconnecting = 0;
	Connect_FB = 1;
	Connect_Status_FB = AudioClient.SocketStatus;
	IF( AudioDevice.MuteRequest )
		SetTXQueue( AudioDevice.CommandLEDOn );
	ELSE
		SetTXQueue( AudioDevice.CommandLEDOff );
	WAIT( 500 )
		SetTXQueue( AudioDevice.CommandPollLED );
}

//Disconnected
SOCKETDISCONNECT AudioClient
{
	AudioDevice.StatusConnected = 0;
	Connect_FB = 0;
	Connect_Status_FB = AudioClient.SocketStatus;   
	IF( AudioDevice.StatusConnectRequest && !AudioDevice.Reconnecting )
	{
		AudioDevice.Reconnecting = 1;
		ConnectDisconnect( 1 );
		WHILE( !AudioDevice.StatusConnected && AudioDevice.Reconnecting )
		{
			WAIT( 3000, ReconnectIP )
				ConnectDisconnect( 1 );
 		}
	}
}
SOCKETSTATUS AudioClient
{ 
	Connect_Status_FB = SOCKETGETSTATUS();
	IF( Connect_Status_FB = 2 )
	{
		Connect_FB = 1;
		AudioDevice.StatusConnected = 1;
	}
	ELSE
	{
		Connect_FB = 0;
		AudioDevice.StatusConnected = 0;
		AudioDevice.Reconnecting = 0;
	}
}

//Feedback
SOCKETRECEIVE AudioClient
{
	STRING lvString[127];
	AudioDevice.RXQueue = AudioDevice.RXQueue + AudioClient.SocketRxBuf;
	CLEARBUFFER( AudioClient.SocketRxBuf );
	WHILE( FIND( ">", AudioDevice.RXQueue, 1 ) )
	{
		lvString = REMOVE( ">", AudioDevice.RXQueue );
		SetDebug( lvString, 1 );
		IF( FIND( "REP DEV_LED_IN_STATE OFF", lvString ) )
		{
			LED_Off_FB = 0;
			LED_On_FB = 1;
			AudioDevice.StatusLED = 1;
		}
		ELSE IF( FIND( "REP DEV_LED_IN_STATE ON", lvString ) )
		{
			LED_On_FB = 0;
			LED_Off_FB = 1;
			AudioDevice.StatusLED = 0;
		}	
		ELSE IF( FIND( "EXT_SWITCH_OUT_STATE OFF", lvString ) )
		{
			Mute_Button_Off_FB = 0;
			Mute_Button_On_FB = 1;		
		}	
		ELSE IF( FIND( "EXT_SWITCH_OUT_STATE ON", lvString ) )
		{
			Mute_Button_On_FB = 0;
			Mute_Button_Off_FB = 1;		
		}
	}
}
  
CHANGE Connect
{
	IF( Connect )
	{	
		AudioDevice.StatusConnectRequest = 1;
		ConnectDisconnect( 1 );
	}
	ELSE
	{
		AudioDevice.StatusConnectRequest = 0;
		ConnectDisconnect( 0 );
	}					
}

CHANGE IP_Address
{
	AudioDevice.IPAddress = IP_Address;
}
CHANGE IP_Port
{
	AudioDevice.IPPort = IP_Port;
}
CHANGE ManualCMD
{
	SetTXQueue( ManualCMD );
}
PUSH LED_On
{
	AudioDevice.MuteRequest = 1;
	SetTXQueue( AudioDevice.CommandLEDOn );
}
PUSH LED_Off
{
	AudioDevice.MuteRequest = 0;
	SetTXQueue( AudioDevice.CommandLEDOff );
}
PUSH Poll
{
	SetTXQueue( AudioDevice.CommandPollLED );
}

/*******************************************************************************************
  Main()
*******************************************************************************************/
FUNCTION Main()
{
	AudioDevice.IPPort = 2202;
	AudioDevice.CommandLEDOn = "SET DEV_LED_IN_STATE OFF";
	AudioDevice.CommandLEDOff = "SET DEV_LED_IN_STATE ON";
	AudioDevice.CommandPollLED = "GET DEV_LED_IN_STATE";
	AudioDevice.CommandPollButtonState = "GET EXT_SWITCH_OUT_STATE";
}
