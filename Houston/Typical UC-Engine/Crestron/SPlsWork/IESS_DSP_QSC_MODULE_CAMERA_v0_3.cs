using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_DSP_QSC_MODULE_CAMERA_V0_3
{
    public class UserModuleClass_IESS_DSP_QSC_MODULE_CAMERA_V0_3 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        Crestron.Logos.SplusObjects.DigitalInput INITIALIZE;
        Crestron.Logos.SplusObjects.DigitalInput POLL;
        Crestron.Logos.SplusObjects.DigitalInput TILT_UP;
        Crestron.Logos.SplusObjects.DigitalInput TILT_DOWN;
        Crestron.Logos.SplusObjects.DigitalInput PAN_LEFT;
        Crestron.Logos.SplusObjects.DigitalInput PAN_RIGHT;
        Crestron.Logos.SplusObjects.DigitalInput ZOOM_IN;
        Crestron.Logos.SplusObjects.DigitalInput ZOOM_OUT;
        Crestron.Logos.SplusObjects.DigitalInput PRIVACY_ON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> LOAD_PRESET;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> SAVE_PRESET;
        Crestron.Logos.SplusObjects.StringInput CONTROL_NAME;
        Crestron.Logos.SplusObjects.StringInput DSP_RX;
        Crestron.Logos.SplusObjects.DigitalOutput CAMPRIVACYSTATUS;
        Crestron.Logos.SplusObjects.StringOutput DSP_TX;
        SPTZCAMERA CAM;
        ushort GVPRESET = 0;
        private void SETCOMMANDVALUE (  SplusExecutionContext __context__, CrestronString INSTRING , ushort INVALUE ) 
            { 
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 512, this );
            
            
            __context__.SourceCodeLine = 90;
            LVSTRING  .UpdateValue ( CAM . COMPSET + INSTRING + CAM . MTX + Functions.ItoA (  (int) ( INVALUE ) ) + CAM . ETX  ) ; 
            __context__.SourceCodeLine = 91;
            DSP_TX  .UpdateValue ( LVSTRING  ) ; 
            
            }
            
        private void SETCOMMANDSTRING (  SplusExecutionContext __context__, CrestronString INSTRING , CrestronString INVALUE ) 
            { 
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 512, this );
            
            
            __context__.SourceCodeLine = 96;
            LVSTRING  .UpdateValue ( CAM . COMPSET + INSTRING + CAM . MTX + "\u0022" + INVALUE + "\u0022" + CAM . ETX  ) ; 
            __context__.SourceCodeLine = 97;
            DSP_TX  .UpdateValue ( LVSTRING  ) ; 
            
            }
            
        private void GETPTZ (  SplusExecutionContext __context__ ) 
            { 
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 512, this );
            
            
            __context__.SourceCodeLine = 102;
            LVSTRING  .UpdateValue ( CAM . COMPGET + CAM . PRESET + CAM . COMPGET2 + CAM . ETX  ) ; 
            __context__.SourceCodeLine = 103;
            DSP_TX  .UpdateValue ( LVSTRING  ) ; 
            
            }
            
        private void POLLPRIVACY (  SplusExecutionContext __context__ ) 
            { 
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 512, this );
            
            
            __context__.SourceCodeLine = 108;
            LVSTRING  .UpdateValue ( CAM . COMPGET + CAM . PRIVACY + CAM . COMPGET2 + CAM . ETX  ) ; 
            __context__.SourceCodeLine = 109;
            DSP_TX  .UpdateValue ( LVSTRING  ) ; 
            
            }
            
        private void SAVEPRESET (  SplusExecutionContext __context__, ushort LVINDEX ) 
            { 
            
            __context__.SourceCodeLine = 113;
            GETPTZ (  __context__  ) ; 
            __context__.SourceCodeLine = 114;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_79__" , 50 , __SPLS_TMPVAR__WAITLABEL_79___Callback ) ;
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_79___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            {
            __context__.SourceCodeLine = 115;
            CAM . PRESETCORDS [ GVPRESET ]  .UpdateValue ( CAM . CURRENTCORDS  ) ; 
            }
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void PARSEFEEDBACK (  SplusExecutionContext __context__ ) 
        { 
        ushort LVINDEX = 0;
        ushort LVNEG = 0;
        
        short LVVOL = 0;
        
        CrestronString LVREMOVE;
        CrestronString LVRX;
        CrestronString LVTRASH;
        CrestronString LVSTRING;
        CrestronString LVCRDSTR;
        CrestronString LVCHAR;
        LVREMOVE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        LVRX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, this );
        LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 8191, this );
        LVCRDSTR  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 32, this );
        LVCHAR  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1, this );
        
        
        __context__.SourceCodeLine = 123;
        while ( Functions.TestForTrue  ( ( Functions.Find( "\u0000" , CAM.RXQUEUE ))  ) ) 
            { 
            __context__.SourceCodeLine = 125;
            LVRX  .UpdateValue ( Functions.Remove ( "\u0000" , CAM . RXQUEUE )  ) ; 
            __context__.SourceCodeLine = 126;
            LVRX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVRX ) - 1), LVRX )  ) ; 
            __context__.SourceCodeLine = 127;
            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022result\u0022" , LVRX ))  ) ) 
                { 
                __context__.SourceCodeLine = 129;
                if ( Functions.TestForTrue  ( ( Functions.Find( CAM.PRESET , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 131;
                    LVREMOVE  .UpdateValue ( CAM . PRESET  ) ; 
                    __context__.SourceCodeLine = 132;
                    LVTRASH  .UpdateValue ( Functions.Remove ( LVREMOVE , LVRX )  ) ; 
                    __context__.SourceCodeLine = 133;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 134;
                    LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 135;
                    LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                    __context__.SourceCodeLine = 136;
                    CAM . CURRENTCORDS  .UpdateValue ( LVSTRING  ) ; 
                    } 
                
                __context__.SourceCodeLine = 138;
                if ( Functions.TestForTrue  ( ( Functions.Find( CAM.PRIVACY , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 140;
                    LVREMOVE  .UpdateValue ( CAM . PRIVACY  ) ; 
                    __context__.SourceCodeLine = 141;
                    LVTRASH  .UpdateValue ( Functions.Remove ( LVREMOVE , LVRX )  ) ; 
                    __context__.SourceCodeLine = 142;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Value\u0022:" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 143;
                    LVSTRING  .UpdateValue ( Functions.Remove ( "." , LVRX )  ) ; 
                    __context__.SourceCodeLine = 144;
                    LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                    __context__.SourceCodeLine = 145;
                    CAMPRIVACYSTATUS  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                    } 
                
                } 
            
            __context__.SourceCodeLine = 123;
            } 
        
        
        }
        
    private void UPDATECOMMANDS (  SplusExecutionContext __context__ ) 
        { 
        
        __context__.SourceCodeLine = 152;
        CAM . COMPSET  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + CAM . CONTROLNAME + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022"  ) ; 
        __context__.SourceCodeLine = 153;
        CAM . COMPGET  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Get\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + CAM . CONTROLNAME + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022"  ) ; 
        __context__.SourceCodeLine = 154;
        CAM . GETPOLL  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Control.Get\u0022, \u0022params\u0022: [\u0022" + CAM . CONTROLNAME + "\u0022]}\u0000"  ) ; 
        
        }
        
    object POLL_OnPush_0 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 161;
            DSP_TX  .UpdateValue ( CAM . GETPOLL  ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object DSP_RX_OnChange_1 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 165;
        CAM . RXQUEUE  .UpdateValue ( CAM . RXQUEUE + DSP_RX  ) ; 
        __context__.SourceCodeLine = 166;
        PARSEFEEDBACK (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CONTROL_NAME_OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 170;
        CAM . CONTROLNAME  .UpdateValue ( CONTROL_NAME  ) ; 
        __context__.SourceCodeLine = 171;
        UPDATECOMMANDS (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TILT_UP_OnChange_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 176;
        if ( Functions.TestForTrue  ( ( Functions.Not( CAM.STATUSPRIVACY ))  ) ) 
            { 
            __context__.SourceCodeLine = 178;
            if ( Functions.TestForTrue  ( ( TILT_UP  .Value)  ) ) 
                {
                __context__.SourceCodeLine = 179;
                SETCOMMANDVALUE (  __context__ , CAM.TILTUP, (ushort)( 1 )) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 181;
                SETCOMMANDVALUE (  __context__ , CAM.TILTUP, (ushort)( 0 )) ; 
                }
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TILT_DOWN_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 187;
        if ( Functions.TestForTrue  ( ( Functions.Not( CAM.STATUSPRIVACY ))  ) ) 
            { 
            __context__.SourceCodeLine = 189;
            if ( Functions.TestForTrue  ( ( TILT_DOWN  .Value)  ) ) 
                {
                __context__.SourceCodeLine = 190;
                SETCOMMANDVALUE (  __context__ , CAM.TILTDOWN, (ushort)( 1 )) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 192;
                SETCOMMANDVALUE (  __context__ , CAM.TILTDOWN, (ushort)( 0 )) ; 
                }
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PAN_LEFT_OnChange_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 198;
        if ( Functions.TestForTrue  ( ( Functions.Not( CAM.STATUSPRIVACY ))  ) ) 
            { 
            __context__.SourceCodeLine = 200;
            if ( Functions.TestForTrue  ( ( PAN_LEFT  .Value)  ) ) 
                {
                __context__.SourceCodeLine = 201;
                SETCOMMANDVALUE (  __context__ , CAM.PANLEFT, (ushort)( 1 )) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 203;
                SETCOMMANDVALUE (  __context__ , CAM.PANLEFT, (ushort)( 0 )) ; 
                }
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PAN_RIGHT_OnChange_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 209;
        if ( Functions.TestForTrue  ( ( Functions.Not( CAM.STATUSPRIVACY ))  ) ) 
            { 
            __context__.SourceCodeLine = 211;
            if ( Functions.TestForTrue  ( ( PAN_RIGHT  .Value)  ) ) 
                {
                __context__.SourceCodeLine = 212;
                SETCOMMANDVALUE (  __context__ , CAM.PANRIGHT, (ushort)( 1 )) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 214;
                SETCOMMANDVALUE (  __context__ , CAM.PANRIGHT, (ushort)( 0 )) ; 
                }
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ZOOM_IN_OnChange_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 220;
        if ( Functions.TestForTrue  ( ( Functions.Not( CAM.STATUSPRIVACY ))  ) ) 
            { 
            __context__.SourceCodeLine = 222;
            if ( Functions.TestForTrue  ( ( ZOOM_IN  .Value)  ) ) 
                {
                __context__.SourceCodeLine = 223;
                SETCOMMANDVALUE (  __context__ , CAM.ZOOMIN, (ushort)( 1 )) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 225;
                SETCOMMANDVALUE (  __context__ , CAM.ZOOMIN, (ushort)( 0 )) ; 
                }
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ZOOM_OUT_OnChange_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 231;
        if ( Functions.TestForTrue  ( ( Functions.Not( CAM.STATUSPRIVACY ))  ) ) 
            { 
            __context__.SourceCodeLine = 233;
            if ( Functions.TestForTrue  ( ( ZOOM_OUT  .Value)  ) ) 
                {
                __context__.SourceCodeLine = 234;
                SETCOMMANDVALUE (  __context__ , CAM.ZOOMOUT, (ushort)( 1 )) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 236;
                SETCOMMANDVALUE (  __context__ , CAM.ZOOMOUT, (ushort)( 0 )) ; 
                }
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PRIVACY_ON_OnChange_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 242;
        if ( Functions.TestForTrue  ( ( PRIVACY_ON  .Value)  ) ) 
            {
            __context__.SourceCodeLine = 243;
            SETCOMMANDVALUE (  __context__ , CAM.PRIVACY, (ushort)( 1 )) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 245;
            SETCOMMANDVALUE (  __context__ , CAM.PRIVACY, (ushort)( 0 )) ; 
            }
        
        __context__.SourceCodeLine = 246;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_80__" , 50 , __SPLS_TMPVAR__WAITLABEL_80___Callback ) ;
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_80___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 247;
            POLLPRIVACY (  __context__  ) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object LOAD_PRESET_OnPush_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 253;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 254;
        GVPRESET = (ushort) ( LVINDEX ) ; 
        __context__.SourceCodeLine = 255;
        SETCOMMANDSTRING (  __context__ , CAM.PRESET, CAM.PRESETCORDS[ GVPRESET ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SAVE_PRESET_OnPush_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 260;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 261;
        GVPRESET = (ushort) ( LVINDEX ) ; 
        __context__.SourceCodeLine = 262;
        SAVEPRESET (  __context__ , (ushort)( GVPRESET )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    ushort LVCOUNTER = 0;
    
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 271;
        CAM . COMPSET  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + CAM . CONTROLNAME + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022"  ) ; 
        __context__.SourceCodeLine = 272;
        CAM . COMPGET  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Get\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + CAM . CONTROLNAME + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022"  ) ; 
        __context__.SourceCodeLine = 273;
        CAM . COMPGET2  .UpdateValue ( "\u0022"  ) ; 
        __context__.SourceCodeLine = 274;
        CAM . GETPOLL  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Control.Get\u0022, \u0022params\u0022: [\u0022" + CAM . CONTROLNAME + "\u0022]}\u0000"  ) ; 
        __context__.SourceCodeLine = 275;
        CAM . MTX  .UpdateValue ( "\u0022, \u0022Value\u0022: "  ) ; 
        __context__.SourceCodeLine = 276;
        CAM . ETX  .UpdateValue ( " }]}}\u0000"  ) ; 
        __context__.SourceCodeLine = 277;
        CAM . PRESET  .UpdateValue ( "ptz.preset"  ) ; 
        __context__.SourceCodeLine = 278;
        CAM . TILTUP  .UpdateValue ( "tilt.up"  ) ; 
        __context__.SourceCodeLine = 279;
        CAM . TILTDOWN  .UpdateValue ( "tilt.down"  ) ; 
        __context__.SourceCodeLine = 280;
        CAM . PANLEFT  .UpdateValue ( "pan.left"  ) ; 
        __context__.SourceCodeLine = 281;
        CAM . PANRIGHT  .UpdateValue ( "pan.right"  ) ; 
        __context__.SourceCodeLine = 282;
        CAM . ZOOMIN  .UpdateValue ( "zoom.in"  ) ; 
        __context__.SourceCodeLine = 283;
        CAM . ZOOMOUT  .UpdateValue ( "zoom.out"  ) ; 
        __context__.SourceCodeLine = 284;
        CAM . PRIVACY  .UpdateValue ( "toggle.privacy"  ) ; 
        __context__.SourceCodeLine = 285;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)8; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 287;
            CAM . PRESETCORDS [ LVCOUNTER ]  .UpdateValue ( "0 0 0"  ) ; 
            __context__.SourceCodeLine = 285;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    CAM  = new SPTZCAMERA( this, true );
    CAM .PopulateCustomAttributeList( false );
    
    INITIALIZE = new Crestron.Logos.SplusObjects.DigitalInput( INITIALIZE__DigitalInput__, this );
    m_DigitalInputList.Add( INITIALIZE__DigitalInput__, INITIALIZE );
    
    POLL = new Crestron.Logos.SplusObjects.DigitalInput( POLL__DigitalInput__, this );
    m_DigitalInputList.Add( POLL__DigitalInput__, POLL );
    
    TILT_UP = new Crestron.Logos.SplusObjects.DigitalInput( TILT_UP__DigitalInput__, this );
    m_DigitalInputList.Add( TILT_UP__DigitalInput__, TILT_UP );
    
    TILT_DOWN = new Crestron.Logos.SplusObjects.DigitalInput( TILT_DOWN__DigitalInput__, this );
    m_DigitalInputList.Add( TILT_DOWN__DigitalInput__, TILT_DOWN );
    
    PAN_LEFT = new Crestron.Logos.SplusObjects.DigitalInput( PAN_LEFT__DigitalInput__, this );
    m_DigitalInputList.Add( PAN_LEFT__DigitalInput__, PAN_LEFT );
    
    PAN_RIGHT = new Crestron.Logos.SplusObjects.DigitalInput( PAN_RIGHT__DigitalInput__, this );
    m_DigitalInputList.Add( PAN_RIGHT__DigitalInput__, PAN_RIGHT );
    
    ZOOM_IN = new Crestron.Logos.SplusObjects.DigitalInput( ZOOM_IN__DigitalInput__, this );
    m_DigitalInputList.Add( ZOOM_IN__DigitalInput__, ZOOM_IN );
    
    ZOOM_OUT = new Crestron.Logos.SplusObjects.DigitalInput( ZOOM_OUT__DigitalInput__, this );
    m_DigitalInputList.Add( ZOOM_OUT__DigitalInput__, ZOOM_OUT );
    
    PRIVACY_ON = new Crestron.Logos.SplusObjects.DigitalInput( PRIVACY_ON__DigitalInput__, this );
    m_DigitalInputList.Add( PRIVACY_ON__DigitalInput__, PRIVACY_ON );
    
    LOAD_PRESET = new InOutArray<DigitalInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        LOAD_PRESET[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( LOAD_PRESET__DigitalInput__ + i, LOAD_PRESET__DigitalInput__, this );
        m_DigitalInputList.Add( LOAD_PRESET__DigitalInput__ + i, LOAD_PRESET[i+1] );
    }
    
    SAVE_PRESET = new InOutArray<DigitalInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        SAVE_PRESET[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( SAVE_PRESET__DigitalInput__ + i, SAVE_PRESET__DigitalInput__, this );
        m_DigitalInputList.Add( SAVE_PRESET__DigitalInput__ + i, SAVE_PRESET[i+1] );
    }
    
    CAMPRIVACYSTATUS = new Crestron.Logos.SplusObjects.DigitalOutput( CAMPRIVACYSTATUS__DigitalOutput__, this );
    m_DigitalOutputList.Add( CAMPRIVACYSTATUS__DigitalOutput__, CAMPRIVACYSTATUS );
    
    CONTROL_NAME = new Crestron.Logos.SplusObjects.StringInput( CONTROL_NAME__AnalogSerialInput__, 64, this );
    m_StringInputList.Add( CONTROL_NAME__AnalogSerialInput__, CONTROL_NAME );
    
    DSP_RX = new Crestron.Logos.SplusObjects.StringInput( DSP_RX__AnalogSerialInput__, 65534, this );
    m_StringInputList.Add( DSP_RX__AnalogSerialInput__, DSP_RX );
    
    DSP_TX = new Crestron.Logos.SplusObjects.StringOutput( DSP_TX__AnalogSerialOutput__, this );
    m_StringOutputList.Add( DSP_TX__AnalogSerialOutput__, DSP_TX );
    
    __SPLS_TMPVAR__WAITLABEL_79___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_79___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_80___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_80___CallbackFn );
    
    POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_OnPush_0, false ) );
    DSP_RX.OnSerialChange.Add( new InputChangeHandlerWrapper( DSP_RX_OnChange_1, false ) );
    CONTROL_NAME.OnSerialChange.Add( new InputChangeHandlerWrapper( CONTROL_NAME_OnChange_2, false ) );
    TILT_UP.OnDigitalChange.Add( new InputChangeHandlerWrapper( TILT_UP_OnChange_3, false ) );
    TILT_DOWN.OnDigitalChange.Add( new InputChangeHandlerWrapper( TILT_DOWN_OnChange_4, false ) );
    PAN_LEFT.OnDigitalChange.Add( new InputChangeHandlerWrapper( PAN_LEFT_OnChange_5, false ) );
    PAN_RIGHT.OnDigitalChange.Add( new InputChangeHandlerWrapper( PAN_RIGHT_OnChange_6, false ) );
    ZOOM_IN.OnDigitalChange.Add( new InputChangeHandlerWrapper( ZOOM_IN_OnChange_7, false ) );
    ZOOM_OUT.OnDigitalChange.Add( new InputChangeHandlerWrapper( ZOOM_OUT_OnChange_8, false ) );
    PRIVACY_ON.OnDigitalChange.Add( new InputChangeHandlerWrapper( PRIVACY_ON_OnChange_9, false ) );
    for( uint i = 0; i < 8; i++ )
        LOAD_PRESET[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( LOAD_PRESET_OnPush_10, false ) );
        
    for( uint i = 0; i < 8; i++ )
        SAVE_PRESET[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( SAVE_PRESET_OnPush_11, false ) );
        
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_DSP_QSC_MODULE_CAMERA_V0_3 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_79___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_80___Callback;


const uint INITIALIZE__DigitalInput__ = 0;
const uint POLL__DigitalInput__ = 1;
const uint TILT_UP__DigitalInput__ = 2;
const uint TILT_DOWN__DigitalInput__ = 3;
const uint PAN_LEFT__DigitalInput__ = 4;
const uint PAN_RIGHT__DigitalInput__ = 5;
const uint ZOOM_IN__DigitalInput__ = 6;
const uint ZOOM_OUT__DigitalInput__ = 7;
const uint PRIVACY_ON__DigitalInput__ = 8;
const uint LOAD_PRESET__DigitalInput__ = 9;
const uint SAVE_PRESET__DigitalInput__ = 17;
const uint CONTROL_NAME__AnalogSerialInput__ = 0;
const uint DSP_RX__AnalogSerialInput__ = 1;
const uint CAMPRIVACYSTATUS__DigitalOutput__ = 0;
const uint DSP_TX__AnalogSerialOutput__ = 0;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SPTZCAMERA : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  STATUSPRIVACY = 0;
    
    [SplusStructAttribute(1, false, false)]
    public CrestronString  RXQUEUE;
    
    [SplusStructAttribute(2, false, false)]
    public CrestronString  CONTROLNAME;
    
    [SplusStructAttribute(3, false, false)]
    public CrestronString  PRESET;
    
    [SplusStructAttribute(4, false, false)]
    public CrestronString  [] PRESETCORDS;
    
    [SplusStructAttribute(5, false, false)]
    public CrestronString  TILTUP;
    
    [SplusStructAttribute(6, false, false)]
    public CrestronString  TILTDOWN;
    
    [SplusStructAttribute(7, false, false)]
    public CrestronString  PANLEFT;
    
    [SplusStructAttribute(8, false, false)]
    public CrestronString  PANRIGHT;
    
    [SplusStructAttribute(9, false, false)]
    public CrestronString  ZOOMIN;
    
    [SplusStructAttribute(10, false, false)]
    public CrestronString  ZOOMOUT;
    
    [SplusStructAttribute(11, false, false)]
    public CrestronString  PRIVACY;
    
    [SplusStructAttribute(12, false, false)]
    public CrestronString  COMPSET;
    
    [SplusStructAttribute(13, false, false)]
    public CrestronString  COMPGET;
    
    [SplusStructAttribute(14, false, false)]
    public CrestronString  COMPGET2;
    
    [SplusStructAttribute(15, false, false)]
    public CrestronString  GETPOLL;
    
    [SplusStructAttribute(16, false, false)]
    public CrestronString  GETPTZ;
    
    [SplusStructAttribute(17, false, false)]
    public CrestronString  CURRENTCORDS;
    
    [SplusStructAttribute(18, false, false)]
    public CrestronString  MTX;
    
    [SplusStructAttribute(19, false, false)]
    public CrestronString  ETX;
    
    
    public SPTZCAMERA( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        RXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, Owner );
        CONTROLNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        PRESET  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 32, Owner );
        TILTUP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        TILTDOWN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        PANLEFT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        PANRIGHT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        ZOOMIN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        ZOOMOUT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        PRIVACY  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        COMPSET  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        COMPGET  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        COMPGET2  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        GETPOLL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        GETPTZ  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        CURRENTCORDS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 64, Owner );
        MTX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 32, Owner );
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16, Owner );
        PRESETCORDS  = new CrestronString[ 9 ];
        for( uint i = 0; i < 9; i++ )
            PRESETCORDS [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 128, Owner );
        
        
    }
    
}

}
