using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_DSP_QSC_MODULE_HIDCONF_V0_1
{
    public class UserModuleClass_IESS_DSP_QSC_MODULE_HIDCONF_V0_1 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        Crestron.Logos.SplusObjects.DigitalInput INITIALIZE;
        Crestron.Logos.SplusObjects.DigitalInput POLL;
        Crestron.Logos.SplusObjects.StringInput CONTROL_NAME;
        Crestron.Logos.SplusObjects.StringInput DSP_RX;
        Crestron.Logos.SplusObjects.DigitalOutput OFF_HOOK;
        Crestron.Logos.SplusObjects.DigitalOutput RINGING;
        Crestron.Logos.SplusObjects.StringOutput DSP_TX;
        SHIDCONF CONF;
        private void PARSEFEEDBACK (  SplusExecutionContext __context__ ) 
            { 
            ushort LVINDEX = 0;
            ushort LVNEG = 0;
            
            short LVVOL = 0;
            
            CrestronString LVREMOVE;
            CrestronString LVRX;
            CrestronString LVTRASH;
            CrestronString LVSTRING;
            CrestronString LVCRDSTR;
            CrestronString LVCHAR;
            LVREMOVE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            LVRX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, this );
            LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 8191, this );
            LVCRDSTR  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 32, this );
            LVCHAR  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1, this );
            
            
            __context__.SourceCodeLine = 79;
            while ( Functions.TestForTrue  ( ( Functions.Find( "\u0000" , CONF.RXQUEUE ))  ) ) 
                { 
                __context__.SourceCodeLine = 81;
                LVRX  .UpdateValue ( Functions.Remove ( "\u0000" , CONF . RXQUEUE )  ) ; 
                __context__.SourceCodeLine = 82;
                LVRX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVRX ) - 1), LVRX )  ) ; 
                __context__.SourceCodeLine = 83;
                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022result\u0022" , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 85;
                    if ( Functions.TestForTrue  ( ( Functions.Find( CONF.GETHOOK , LVRX ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 87;
                        LVREMOVE  .UpdateValue ( CONF . GETHOOK  ) ; 
                        __context__.SourceCodeLine = 88;
                        LVTRASH  .UpdateValue ( Functions.Remove ( LVREMOVE , LVRX )  ) ; 
                        __context__.SourceCodeLine = 89;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Value\u0022:" , LVRX )  ) ; 
                        __context__.SourceCodeLine = 90;
                        LVSTRING  .UpdateValue ( Functions.Remove ( 1, LVRX )  ) ; 
                        __context__.SourceCodeLine = 91;
                        OFF_HOOK  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 93;
                        if ( Functions.TestForTrue  ( ( Functions.Find( CONF.GETRING , LVRX ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 95;
                            LVREMOVE  .UpdateValue ( CONF . GETRING  ) ; 
                            __context__.SourceCodeLine = 96;
                            LVTRASH  .UpdateValue ( Functions.Remove ( LVREMOVE , LVRX )  ) ; 
                            __context__.SourceCodeLine = 97;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Value\u0022:" , LVRX )  ) ; 
                            __context__.SourceCodeLine = 98;
                            LVSTRING  .UpdateValue ( Functions.Remove ( 1, LVRX )  ) ; 
                            __context__.SourceCodeLine = 99;
                            RINGING  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                            } 
                        
                        }
                    
                    } 
                
                __context__.SourceCodeLine = 79;
                } 
            
            
            }
            
        private void UPDATECOMMANDS (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 107;
            CONF . COMPGET  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Get\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + CONF . CONTROLNAME + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022"  ) ; 
            
            }
            
        private void POLLHOOK (  SplusExecutionContext __context__ ) 
            { 
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            
            __context__.SourceCodeLine = 112;
            LVSTRING  .UpdateValue ( CONF . COMPGET + CONF . GETHOOK + CONF . COMPGET2 + CONF . ETX  ) ; 
            __context__.SourceCodeLine = 113;
            DSP_TX  .UpdateValue ( LVSTRING  ) ; 
            
            }
            
        private void POLLRING (  SplusExecutionContext __context__ ) 
            { 
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            
            __context__.SourceCodeLine = 118;
            LVSTRING  .UpdateValue ( CONF . COMPGET + CONF . GETRING + CONF . COMPGET2 + CONF . ETX  ) ; 
            __context__.SourceCodeLine = 119;
            DSP_TX  .UpdateValue ( LVSTRING  ) ; 
            
            }
            
        object INITIALIZE_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                CrestronString LVSTRING;
                LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1024, this );
                
                
                __context__.SourceCodeLine = 128;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022ChangeGroup.AddComponentControl\u0022, \u0022params\u0022: { \u0022Id\u0022: \u0022MainGroup\u0022, \u0022Component\u0022 : { \u0022Name\u0022: \u0022" + CONF . CONTROLNAME + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022" + CONF . GETHOOK + "\u0022 }, { \u0022Name\u0022: \u0022" + CONF . GETRING + "\u0022 } ] } } }"  ) ; 
                __context__.SourceCodeLine = 129;
                DSP_TX  .UpdateValue ( LVSTRING  ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object POLL_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 133;
            POLLHOOK (  __context__  ) ; 
            __context__.SourceCodeLine = 134;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_81__" , 50 , __SPLS_TMPVAR__WAITLABEL_81___Callback ) ;
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
public void __SPLS_TMPVAR__WAITLABEL_81___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 135;
            POLLRING (  __context__  ) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object DSP_RX_OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 139;
        CONF . RXQUEUE  .UpdateValue ( CONF . RXQUEUE + DSP_RX  ) ; 
        __context__.SourceCodeLine = 140;
        PARSEFEEDBACK (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CONTROL_NAME_OnChange_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 144;
        CONF . CONTROLNAME  .UpdateValue ( CONTROL_NAME  ) ; 
        __context__.SourceCodeLine = 145;
        UPDATECOMMANDS (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 152;
        CONF . COMPGET  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Get\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + CONF . CONTROLNAME + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022"  ) ; 
        __context__.SourceCodeLine = 153;
        CONF . COMPGET2  .UpdateValue ( "\u0022"  ) ; 
        __context__.SourceCodeLine = 154;
        CONF . ETX  .UpdateValue ( " }]}}\u0000"  ) ; 
        __context__.SourceCodeLine = 155;
        CONF . GETHOOK  .UpdateValue ( "spk.led.off.hook"  ) ; 
        __context__.SourceCodeLine = 156;
        CONF . GETRING  .UpdateValue ( "spk.led.ring"  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    CONF  = new SHIDCONF( this, true );
    CONF .PopulateCustomAttributeList( false );
    
    INITIALIZE = new Crestron.Logos.SplusObjects.DigitalInput( INITIALIZE__DigitalInput__, this );
    m_DigitalInputList.Add( INITIALIZE__DigitalInput__, INITIALIZE );
    
    POLL = new Crestron.Logos.SplusObjects.DigitalInput( POLL__DigitalInput__, this );
    m_DigitalInputList.Add( POLL__DigitalInput__, POLL );
    
    OFF_HOOK = new Crestron.Logos.SplusObjects.DigitalOutput( OFF_HOOK__DigitalOutput__, this );
    m_DigitalOutputList.Add( OFF_HOOK__DigitalOutput__, OFF_HOOK );
    
    RINGING = new Crestron.Logos.SplusObjects.DigitalOutput( RINGING__DigitalOutput__, this );
    m_DigitalOutputList.Add( RINGING__DigitalOutput__, RINGING );
    
    CONTROL_NAME = new Crestron.Logos.SplusObjects.StringInput( CONTROL_NAME__AnalogSerialInput__, 64, this );
    m_StringInputList.Add( CONTROL_NAME__AnalogSerialInput__, CONTROL_NAME );
    
    DSP_RX = new Crestron.Logos.SplusObjects.StringInput( DSP_RX__AnalogSerialInput__, 65534, this );
    m_StringInputList.Add( DSP_RX__AnalogSerialInput__, DSP_RX );
    
    DSP_TX = new Crestron.Logos.SplusObjects.StringOutput( DSP_TX__AnalogSerialOutput__, this );
    m_StringOutputList.Add( DSP_TX__AnalogSerialOutput__, DSP_TX );
    
    __SPLS_TMPVAR__WAITLABEL_81___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_81___CallbackFn );
    
    INITIALIZE.OnDigitalPush.Add( new InputChangeHandlerWrapper( INITIALIZE_OnPush_0, false ) );
    POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_OnPush_1, false ) );
    DSP_RX.OnSerialChange.Add( new InputChangeHandlerWrapper( DSP_RX_OnChange_2, false ) );
    CONTROL_NAME.OnSerialChange.Add( new InputChangeHandlerWrapper( CONTROL_NAME_OnChange_3, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_DSP_QSC_MODULE_HIDCONF_V0_1 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_81___Callback;


const uint INITIALIZE__DigitalInput__ = 0;
const uint POLL__DigitalInput__ = 1;
const uint CONTROL_NAME__AnalogSerialInput__ = 0;
const uint DSP_RX__AnalogSerialInput__ = 1;
const uint OFF_HOOK__DigitalOutput__ = 0;
const uint RINGING__DigitalOutput__ = 1;
const uint DSP_TX__AnalogSerialOutput__ = 0;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SHIDCONF : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  OFFHOOK = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  RINGING = 0;
    
    [SplusStructAttribute(2, false, false)]
    public CrestronString  RXQUEUE;
    
    [SplusStructAttribute(3, false, false)]
    public CrestronString  CONTROLNAME;
    
    [SplusStructAttribute(4, false, false)]
    public CrestronString  GETHOOK;
    
    [SplusStructAttribute(5, false, false)]
    public CrestronString  GETRING;
    
    [SplusStructAttribute(6, false, false)]
    public CrestronString  COMPGET;
    
    [SplusStructAttribute(7, false, false)]
    public CrestronString  COMPGET2;
    
    [SplusStructAttribute(8, false, false)]
    public CrestronString  MTX;
    
    [SplusStructAttribute(9, false, false)]
    public CrestronString  ETX;
    
    
    public SHIDCONF( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        RXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, Owner );
        CONTROLNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        GETHOOK  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        GETRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        COMPGET  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        COMPGET2  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        MTX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 32, Owner );
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16, Owner );
        
        
    }
    
}

}
