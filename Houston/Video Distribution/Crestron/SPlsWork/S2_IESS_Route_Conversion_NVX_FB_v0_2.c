#include "TypeDefs.h"
#include "Globals.h"
#include "FnctList.h"
#include "Library.h"
#include "SimplSig.h"
#include "S2_IESS_Route_Conversion_NVX_FB_v0_2.h"

FUNCTION_MAIN( S2_IESS_Route_Conversion_NVX_FB_v0_2 );
EVENT_HANDLER( S2_IESS_Route_Conversion_NVX_FB_v0_2 );
DEFINE_ENTRYPOINT( S2_IESS_Route_Conversion_NVX_FB_v0_2 );








DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Route_Conversion_NVX_FB_v0_2, 00000 /*SwitcherFB_Output*/ )

    {
    /* Begin local function variable declarations */
    
    unsigned short  __LVINDEX; 
    unsigned short  __LVCOUNTER; 
    CREATE_STRING_STRUCT( S2_IESS_Route_Conversion_NVX_FB_v0_2, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Route_Conversion_NVX_FB_v0_2, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Route_Conversion_NVX_FB_v0_2, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "" );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 76 );
    __LVINDEX = GetLocalLastModifiedArrayIndex ( S2_IESS_Route_Conversion_NVX_FB_v0_2 ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 77 );
    if ( (CompareStrings( GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), GLOBAL_STRING_ARRAY( S2_IESS_Route_Conversion_NVX_FB_v0_2, __SWITCHERFB_OUTPUT  )  ,  __LVINDEX  ), LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ ) , 1 ) == 0)) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 79 );
        Globals->S2_IESS_Route_Conversion_NVX_FB_v0_2.__GVLASTSELECTED = 0; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 80 );
        SET_GLOBAL_INTARRAY_VALUE( S2_IESS_Route_Conversion_NVX_FB_v0_2, __GVSWITCHEROUTPUT, 0, __LVINDEX , 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 81 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Route_Conversion_NVX_FB_v0_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SetSerial( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), __S2_IESS_Route_Conversion_NVX_FB_v0_2_LASTSELECTEDINPUTSOURCENAME_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) );
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 82 );
        SetAnalog ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), __S2_IESS_Route_Conversion_NVX_FB_v0_2_LASTSELECTEDINPUT_ANALOG_OUTPUT, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 83 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), &Globals->S2_IESS_Route_Conversion_NVX_FB_v0_2.__SWITCHERSOURCE ,__LVINDEX, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 84 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Route_Conversion_NVX_FB_v0_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Route_Conversion_NVX_FB_v0_2, __VIDEOOUTPUTSOURCENAME , __LVINDEX ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ); }
        
        ; 
        } 
    
    else 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 88 );
        __FN_FOREND_VAL__1 = 16; 
        __FN_FORINIT_VAL__1 = 1; 
        for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 90 );
            if ( (CompareStrings( GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), GLOBAL_STRING_ARRAY( S2_IESS_Route_Conversion_NVX_FB_v0_2, __SWITCHERFB_OUTPUT  )  ,  __LVINDEX  ), GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), GLOBAL_STRING_ARRAY( S2_IESS_Route_Conversion_NVX_FB_v0_2, __SOURCEINPUT  )  ,  __LVCOUNTER  ), 1 ) == 0)) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 92 );
                Globals->S2_IESS_Route_Conversion_NVX_FB_v0_2.__GVLASTSELECTED = __LVINDEX; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 93 );
                SET_GLOBAL_INTARRAY_VALUE( S2_IESS_Route_Conversion_NVX_FB_v0_2, __GVSWITCHEROUTPUT, 0, __LVINDEX , __LVCOUNTER) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 94 );
                if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ) == 0 ) {
                FormatString ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Route_Conversion_NVX_FB_v0_2 )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Route_Conversion_NVX_FB_v0_2, __INPUTNAMES  )    ,  __LVCOUNTER  )  )  ; 
                SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Route_Conversion_NVX_FB_v0_2, __VIDEOOUTPUTSOURCENAME , __LVINDEX ) ; 
                ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ); }
                
                ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 95 );
                if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ) == 0 ) {
                FormatString ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Route_Conversion_NVX_FB_v0_2 )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Route_Conversion_NVX_FB_v0_2, __INPUTNAMES  )    ,  __LVCOUNTER  )  )  ; 
                SetSerial( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), __S2_IESS_Route_Conversion_NVX_FB_v0_2_LASTSELECTEDINPUTSOURCENAME_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) );
                ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ); }
                
                ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 96 );
                SetAnalog ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), __S2_IESS_Route_Conversion_NVX_FB_v0_2_LASTSELECTEDINPUT_ANALOG_OUTPUT, __LVCOUNTER) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 97 );
                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), &Globals->S2_IESS_Route_Conversion_NVX_FB_v0_2.__SWITCHERSOURCE ,__LVINDEX, __LVCOUNTER) ; 
                } 
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 88 );
            } 
        
        } 
    
    
    S2_IESS_Route_Conversion_NVX_FB_v0_2_Exit__Event_0:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Route_Conversion_NVX_FB_v0_2, 00001 /*InputNames*/ )

    {
    /* Begin local function variable declarations */
    
    unsigned short  __LVINDEX; 
    unsigned short  __LVCOUNTER; 
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 105 );
    __LVINDEX = GetLocalLastModifiedArrayIndex ( S2_IESS_Route_Conversion_NVX_FB_v0_2 ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 106 );
    __FN_FOREND_VAL__1 = 32; 
    __FN_FORINIT_VAL__1 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 108 );
        if ( (GET_GLOBAL_INTARRAY_VALUE( S2_IESS_Route_Conversion_NVX_FB_v0_2, __GVSWITCHEROUTPUT, 0, __LVCOUNTER  ) == __LVINDEX)) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 109 );
            if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ) == 0 ) {
            FormatString ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Route_Conversion_NVX_FB_v0_2 )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Route_Conversion_NVX_FB_v0_2, __INPUTNAMES  )    ,  __LVINDEX  )  )  ; 
            SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Route_Conversion_NVX_FB_v0_2, __VIDEOOUTPUTSOURCENAME , __LVCOUNTER ) ; 
            ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ); }
            
            ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 106 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 111 );
    if ( (Globals->S2_IESS_Route_Conversion_NVX_FB_v0_2.__GVLASTSELECTED == __LVINDEX)) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), 112 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Route_Conversion_NVX_FB_v0_2 )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Route_Conversion_NVX_FB_v0_2, __INPUTNAMES  )    ,  __LVINDEX  )  )  ; 
        SetSerial( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ), __S2_IESS_Route_Conversion_NVX_FB_v0_2_LASTSELECTEDINPUTSOURCENAME_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) );
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ); }
        
        ; 
        }
    
    
    S2_IESS_Route_Conversion_NVX_FB_v0_2_Exit__Event_1:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}


/********************************************************************************
  Constructor
********************************************************************************/

/********************************************************************************
  Destructor
********************************************************************************/

/********************************************************************************
  static void ProcessDigitalEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessDigitalEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessAnalogEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessAnalogEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessStringEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessStringEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_Route_Conversion_NVX_FB_v0_2_SWITCHERFB_OUTPUT_STRING_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_Route_Conversion_NVX_FB_v0_2, 00000 /*SwitcherFB_Output*/, 0 );
            break;
            
        case __S2_IESS_Route_Conversion_NVX_FB_v0_2_INPUTNAMES_STRING_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_Route_Conversion_NVX_FB_v0_2, 00001 /*InputNames*/, 0 );
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketConnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketConnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketStatusEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketStatusEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_FB_v0_2 ) ); 
            break ;
        
    }
}

/********************************************************************************
  EVENT_HANDLER( S2_IESS_Route_Conversion_NVX_FB_v0_2 )
********************************************************************************/
EVENT_HANDLER( S2_IESS_Route_Conversion_NVX_FB_v0_2 )
    {
    SAVE_GLOBAL_POINTERS ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Route_Conversion_NVX_FB_v0_2, __SWITCHERFB_OUTPUT ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Route_Conversion_NVX_FB_v0_2, __SOURCEINPUT ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Route_Conversion_NVX_FB_v0_2, __INPUTNAMES ) ;
    switch ( Signal->Type )
        {
        case e_SIGNAL_TYPE_DIGITAL :
            ProcessDigitalEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_ANALOG :
            ProcessAnalogEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STRING :
            ProcessStringEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_CONNECT :
            ProcessSocketConnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_DISCONNECT :
            ProcessSocketDisconnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_RECEIVE :
            ProcessSocketReceiveEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STATUS :
            ProcessSocketStatusEvent( Signal );
            break ;
        }
        
    RESTORE_GLOBAL_POINTERS ;
    
    }
    
/********************************************************************************
  FUNCTION_MAIN( S2_IESS_Route_Conversion_NVX_FB_v0_2 )
********************************************************************************/
FUNCTION_MAIN( S2_IESS_Route_Conversion_NVX_FB_v0_2 )
{
    SAVE_GLOBAL_POINTERS ;
    
    SET_INSTANCE_POINTER ( S2_IESS_Route_Conversion_NVX_FB_v0_2 );
    INITIALIZE_IO_ARRAY ( S2_IESS_Route_Conversion_NVX_FB_v0_2, __SWITCHERSOURCE, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Route_Conversion_NVX_FB_v0_2_SWITCHERSOURCE_ANALOG_OUTPUT, __S2_IESS_Route_Conversion_NVX_FB_v0_2_SWITCHERSOURCE_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Route_Conversion_NVX_FB_v0_2, __VIDEOOUTPUTSOURCENAME, IO_TYPE_STRING_OUTPUT, __S2_IESS_Route_Conversion_NVX_FB_v0_2_VIDEOOUTPUTSOURCENAME_STRING_OUTPUT, __S2_IESS_Route_Conversion_NVX_FB_v0_2_VIDEOOUTPUTSOURCENAME_ARRAY_LENGTH );
    
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Route_Conversion_NVX_FB_v0_2, __SWITCHERFB_OUTPUT, e_INPUT_TYPE_STRING, __S2_IESS_Route_Conversion_NVX_FB_v0_2_SWITCHERFB_OUTPUT_ARRAY_NUM_ELEMS, __S2_IESS_Route_Conversion_NVX_FB_v0_2_SWITCHERFB_OUTPUT_ARRAY_NUM_CHARS, __S2_IESS_Route_Conversion_NVX_FB_v0_2_SWITCHERFB_OUTPUT_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Route_Conversion_NVX_FB_v0_2, __SWITCHERFB_OUTPUT );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Route_Conversion_NVX_FB_v0_2, __SOURCEINPUT, e_INPUT_TYPE_STRING, __S2_IESS_Route_Conversion_NVX_FB_v0_2_SOURCEINPUT_ARRAY_NUM_ELEMS, __S2_IESS_Route_Conversion_NVX_FB_v0_2_SOURCEINPUT_ARRAY_NUM_CHARS, __S2_IESS_Route_Conversion_NVX_FB_v0_2_SOURCEINPUT_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Route_Conversion_NVX_FB_v0_2, __SOURCEINPUT );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Route_Conversion_NVX_FB_v0_2, __INPUTNAMES, e_INPUT_TYPE_STRING, __S2_IESS_Route_Conversion_NVX_FB_v0_2_INPUTNAMES_ARRAY_NUM_ELEMS, __S2_IESS_Route_Conversion_NVX_FB_v0_2_INPUTNAMES_ARRAY_NUM_CHARS, __S2_IESS_Route_Conversion_NVX_FB_v0_2_INPUTNAMES_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Route_Conversion_NVX_FB_v0_2, __INPUTNAMES );
    
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_Route_Conversion_NVX_FB_v0_2, sGenericOutStr, e_INPUT_TYPE_NONE, GENERIC_STRING_OUTPUT_LEN );
    
    INITIALIZE_GLOBAL_INTARRAY ( S2_IESS_Route_Conversion_NVX_FB_v0_2, __GVSWITCHEROUTPUT, 0, 32 );
    
    
    S2_IESS_Route_Conversion_NVX_FB_v0_2_Exit__MAIN:
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }
