#include "TypeDefs.h"
#include "Globals.h"
#include "FnctList.h"
#include "Library.h"
#include "SimplSig.h"
#include "S2_IESS_Display_Routing_v0_7.h"

FUNCTION_MAIN( S2_IESS_Display_Routing_v0_7 );
EVENT_HANDLER( S2_IESS_Display_Routing_v0_7 );
DEFINE_ENTRYPOINT( S2_IESS_Display_Routing_v0_7 );


static void S2_IESS_Display_Routing_v0_7__POPUP ( ) 
    { 
    /* Begin local function variable declarations */
    
    CheckStack( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 83 );
    SetDigital ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), __S2_IESS_Display_Routing_v0_7_DISPLAYPOPUP_DIG_OUTPUT, 1) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 84 );
    Globals->S2_IESS_Display_Routing_v0_7.__GLBL_COUNTDOWN = 5; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 85 );
    if ( !( Globals->S2_IESS_Display_Routing_v0_7.__GLBL_POPUPACTIVE )) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 87 );
        Globals->S2_IESS_Display_Routing_v0_7.__GLBL_POPUPACTIVE = 1; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 88 );
        while ( GetDigitalOutput( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), __S2_IESS_Display_Routing_v0_7_DISPLAYPOPUP_DIG_OUTPUT )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 90 );
            CREATE_WAIT( S2_IESS_Display_Routing_v0_7, 100, __SPLS_TMPVAR__WAITLABEL_0__ );
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 88 );
            } 
        
        } 
    
    
    S2_IESS_Display_Routing_v0_7_Exit__POPUP:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    }
    
DEFINE_WAITEVENT( S2_IESS_Display_Routing_v0_7, __SPLS_TMPVAR__WAITLABEL_0__ )
    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 92 );
    Globals->S2_IESS_Display_Routing_v0_7.__GLBL_COUNTDOWN = (Globals->S2_IESS_Display_Routing_v0_7.__GLBL_COUNTDOWN - 1); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 93 );
    if ( (Globals->S2_IESS_Display_Routing_v0_7.__GLBL_COUNTDOWN < 1)) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 95 );
        SetDigital ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), __S2_IESS_Display_Routing_v0_7_DISPLAYPOPUP_DIG_OUTPUT, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 96 );
        Globals->S2_IESS_Display_Routing_v0_7.__GLBL_POPUPACTIVE = 0; 
        } 
    
    

S2_IESS_Display_Routing_v0_7_Exit____SPLS_TMPVAR__WAITLABEL_0__:
    
    /* Begin Free local function variables */
    /* End Free local function variables */
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }
static void S2_IESS_Display_Routing_v0_7__ROUTEALLDISPLAYS ( ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __LVCOUNTER; 
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    CheckStack( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 105 );
    __FN_FOREND_VAL__1 = Globals->S2_IESS_Display_Routing_v0_7.__GLBL_NUMBERDISPLAYS; 
    __FN_FORINIT_VAL__1 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 107 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), &Globals->S2_IESS_Display_Routing_v0_7.__VIDEOSWITCH ,__LVCOUNTER, Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTINPUT) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 105 );
        } 
    
    
    S2_IESS_Display_Routing_v0_7_Exit__ROUTEALLDISPLAYS:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    }
    
DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Display_Routing_v0_7, 00000 /*Inputs*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 116 );
    Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTINPUT = GetLocalLastModifiedArrayIndex ( S2_IESS_Display_Routing_v0_7 ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 117 );
    SetAnalog ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), __S2_IESS_Display_Routing_v0_7_LASTSELECTEDINPUT_ANALOG_OUTPUT, Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTINPUT) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 119 );
    if ( (Globals->S2_IESS_Display_Routing_v0_7.__GLBL_ROUTINGMODE == 1)) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 120 );
        S2_IESS_Display_Routing_v0_7__ROUTEALLDISPLAYS ( ) ; 
        }
    
    else 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 121 );
        if ( (Globals->S2_IESS_Display_Routing_v0_7.__GLBL_ROUTINGMODE == 3)) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 123 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), &Globals->S2_IESS_Display_Routing_v0_7.__VIDEOSWITCH ,Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTOUTPUT, Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTINPUT) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 124 );
            SET_GLOBAL_INTARRAY_VALUE( S2_IESS_Display_Routing_v0_7, __GLBL_OUTPUTS, 0, Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTOUTPUT , Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTINPUT) ; 
            } 
        
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 126 );
    CREATE_WAIT( S2_IESS_Display_Routing_v0_7, 400, SOURCEHOLD );
    
    
    S2_IESS_Display_Routing_v0_7_Exit__Event_0:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_WAITEVENT( S2_IESS_Display_Routing_v0_7, SOURCEHOLD )
    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 128 );
    if ( GetInOutArrayElement( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), &Globals->S2_IESS_Display_Routing_v0_7.__INPUTS , Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTINPUT  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 129 );
        S2_IESS_Display_Routing_v0_7__ROUTEALLDISPLAYS ( ) ; 
        }
    
    

S2_IESS_Display_Routing_v0_7_Exit__SOURCEHOLD:
    
    /* Begin Free local function variables */
    /* End Free local function variables */
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }
DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Display_Routing_v0_7, 00001 /*Inputs*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 134 );
    CancelWait ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), "SOURCEHOLD" ) ; 
    
    S2_IESS_Display_Routing_v0_7_Exit__Event_1:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Display_Routing_v0_7, 00002 /*NumberDisplays*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 138 );
    Globals->S2_IESS_Display_Routing_v0_7.__GLBL_NUMBERDISPLAYS = GetAnalogInput( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), __S2_IESS_Display_Routing_v0_7_NUMBERDISPLAYS_ANALOG_INPUT ); 
    
    S2_IESS_Display_Routing_v0_7_Exit__Event_2:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Display_Routing_v0_7, 00003 /*RoutingMode*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 142 );
    Globals->S2_IESS_Display_Routing_v0_7.__GLBL_ROUTINGMODE = GetAnalogInput( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), __S2_IESS_Display_Routing_v0_7_ROUTINGMODE_ANALOG_INPUT ); 
    
    S2_IESS_Display_Routing_v0_7_Exit__Event_3:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Display_Routing_v0_7, 00004 /*Outputs*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 146 );
    Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTOUTPUT = GetLocalLastModifiedArrayIndex ( S2_IESS_Display_Routing_v0_7 ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 148 );
    if ( (Globals->S2_IESS_Display_Routing_v0_7.__GLBL_ROUTINGMODE == 2)) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 150 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), &Globals->S2_IESS_Display_Routing_v0_7.__VIDEOSWITCH ,Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTOUTPUT, Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTINPUT) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 151 );
        SET_GLOBAL_INTARRAY_VALUE( S2_IESS_Display_Routing_v0_7, __GLBL_OUTPUTS, 0, Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTOUTPUT , Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTINPUT) ; 
        } 
    
    
    S2_IESS_Display_Routing_v0_7_Exit__Event_4:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Display_Routing_v0_7, 00005 /*PowerOff*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 156 );
    Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTINPUT = 0; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 157 );
    SetAnalog ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), __S2_IESS_Display_Routing_v0_7_LASTSELECTEDINPUT_ANALOG_OUTPUT, 0) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 159 );
    if ( (Globals->S2_IESS_Display_Routing_v0_7.__GLBL_ROUTINGMODE == 1)) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 160 );
        S2_IESS_Display_Routing_v0_7__ROUTEALLDISPLAYS ( ) ; 
        }
    
    else 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 161 );
        if ( (Globals->S2_IESS_Display_Routing_v0_7.__GLBL_ROUTINGMODE == 3)) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 163 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), &Globals->S2_IESS_Display_Routing_v0_7.__VIDEOSWITCH ,Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTOUTPUT, Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTINPUT) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 164 );
            SET_GLOBAL_INTARRAY_VALUE( S2_IESS_Display_Routing_v0_7, __GLBL_OUTPUTS, 0, Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTOUTPUT , Globals->S2_IESS_Display_Routing_v0_7.__GLBL_LASTINPUT) ; 
            } 
        
        }
    
    
    S2_IESS_Display_Routing_v0_7_Exit__Event_5:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}


    
    

/********************************************************************************
  Constructor
********************************************************************************/

/********************************************************************************
  Destructor
********************************************************************************/

/********************************************************************************
  static void ProcessDigitalEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessDigitalEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_Display_Routing_v0_7_POWEROFF_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_Display_Routing_v0_7, 00005 /*PowerOff*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) ); 
                
            }
            break;
            
        case __S2_IESS_Display_Routing_v0_7_INPUTS_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_Display_Routing_v0_7, 00000 /*Inputs*/, 0 );
                
            }
            else /*Release*/
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_Display_Routing_v0_7, 00001 /*Inputs*/, 0 );
                
            }
            break;
            
        case __S2_IESS_Display_Routing_v0_7_OUTPUTS_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_Display_Routing_v0_7, 00004 /*Outputs*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) ); 
                
            }
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessAnalogEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessAnalogEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_Display_Routing_v0_7_NUMBERDISPLAYS_ANALOG_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_Display_Routing_v0_7, 00002 /*NumberDisplays*/, 0 );
            break;
            
        case __S2_IESS_Display_Routing_v0_7_ROUTINGMODE_ANALOG_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_Display_Routing_v0_7, 00003 /*RoutingMode*/, 0 );
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessStringEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessStringEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_IESS_Display_Routing_v0_7 ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketConnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketConnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_IESS_Display_Routing_v0_7 ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketStatusEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketStatusEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ) ); 
            break ;
        
    }
}

/********************************************************************************
  EVENT_HANDLER( S2_IESS_Display_Routing_v0_7 )
********************************************************************************/
EVENT_HANDLER( S2_IESS_Display_Routing_v0_7 )
    {
    SAVE_GLOBAL_POINTERS ;
    CHECK_INPUT_ARRAY ( S2_IESS_Display_Routing_v0_7, __INPUTS ) ;
    CHECK_INPUT_ARRAY ( S2_IESS_Display_Routing_v0_7, __OUTPUTS ) ;
    switch ( Signal->Type )
        {
        case e_SIGNAL_TYPE_DIGITAL :
            ProcessDigitalEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_ANALOG :
            ProcessAnalogEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STRING :
            ProcessStringEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_CONNECT :
            ProcessSocketConnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_DISCONNECT :
            ProcessSocketDisconnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_RECEIVE :
            ProcessSocketReceiveEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STATUS :
            ProcessSocketStatusEvent( Signal );
            break ;
        }
        
    RESTORE_GLOBAL_POINTERS ;
    
    }
    
/********************************************************************************
  FUNCTION_MAIN( S2_IESS_Display_Routing_v0_7 )
********************************************************************************/
FUNCTION_MAIN( S2_IESS_Display_Routing_v0_7 )
{
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    SET_INSTANCE_POINTER ( S2_IESS_Display_Routing_v0_7 );
    
    
    /* End local function variable declarations */
    
    INITIALIZE_IO_ARRAY ( S2_IESS_Display_Routing_v0_7, __INPUTS, IO_TYPE_DIGITAL_INPUT, __S2_IESS_Display_Routing_v0_7_INPUTS_DIG_INPUT, __S2_IESS_Display_Routing_v0_7_INPUTS_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Display_Routing_v0_7, __OUTPUTS, IO_TYPE_DIGITAL_INPUT, __S2_IESS_Display_Routing_v0_7_OUTPUTS_DIG_INPUT, __S2_IESS_Display_Routing_v0_7_OUTPUTS_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Display_Routing_v0_7, __VIDEOSWITCH, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Display_Routing_v0_7_VIDEOSWITCH_ANALOG_OUTPUT, __S2_IESS_Display_Routing_v0_7_VIDEOSWITCH_ARRAY_LENGTH );
    
    
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_Display_Routing_v0_7, sGenericOutStr, e_INPUT_TYPE_NONE, GENERIC_STRING_OUTPUT_LEN );
    
    INITIALIZE_GLOBAL_INTARRAY ( S2_IESS_Display_Routing_v0_7, __GLBL_OUTPUTS, 0, 32 );
    
    
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 174 );
    Globals->S2_IESS_Display_Routing_v0_7.__GLBL_POPUPACTIVE = 0; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_7 ), 175 );
    Globals->S2_IESS_Display_Routing_v0_7.__GLBL_ROUTINGMODE = 2; 
    
    S2_IESS_Display_Routing_v0_7_Exit__MAIN:/* Begin Free local function variables */
    /* End Free local function variables */
    
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }
