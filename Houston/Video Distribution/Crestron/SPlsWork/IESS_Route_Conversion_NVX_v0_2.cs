using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_ROUTE_CONVERSION_NVX_V0_2
{
    public class UserModuleClass_IESS_ROUTE_CONVERSION_NVX_V0_2 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> SOURCEFORDISPLAY;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> SOURCEINPUT;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VIDEOOUTPUTSOURCE;
        object SOURCEFORDISPLAY_OnChange_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                ushort LVINDEX = 0;
                
                
                __context__.SourceCodeLine = 74;
                LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
                __context__.SourceCodeLine = 75;
                if ( Functions.TestForTrue  ( ( SOURCEFORDISPLAY[ LVINDEX ] .UshortValue)  ) ) 
                    {
                    __context__.SourceCodeLine = 76;
                    VIDEOOUTPUTSOURCE [ LVINDEX]  .UpdateValue ( SOURCEINPUT [ SOURCEFORDISPLAY[ LVINDEX ] .UshortValue ]  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 78;
                    VIDEOOUTPUTSOURCE [ LVINDEX]  .UpdateValue ( ""  ) ; 
                    }
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    public override object FunctionMain (  object __obj__ ) 
        { 
        try
        {
            SplusExecutionContext __context__ = SplusFunctionMainStartCode();
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler(); }
        return __obj__;
        }
        
    
    public override void LogosSplusInitialize()
    {
        SocketInfo __socketinfo__ = new SocketInfo( 1, this );
        InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
        _SplusNVRAM = new SplusNVRAM( this );
        
        SOURCEFORDISPLAY = new InOutArray<AnalogInput>( 32, this );
        for( uint i = 0; i < 32; i++ )
        {
            SOURCEFORDISPLAY[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( SOURCEFORDISPLAY__AnalogSerialInput__ + i, SOURCEFORDISPLAY__AnalogSerialInput__, this );
            m_AnalogInputList.Add( SOURCEFORDISPLAY__AnalogSerialInput__ + i, SOURCEFORDISPLAY[i+1] );
        }
        
        SOURCEINPUT = new InOutArray<StringInput>( 16, this );
        for( uint i = 0; i < 16; i++ )
        {
            SOURCEINPUT[i+1] = new Crestron.Logos.SplusObjects.StringInput( SOURCEINPUT__AnalogSerialInput__ + i, SOURCEINPUT__AnalogSerialInput__, 63, this );
            m_StringInputList.Add( SOURCEINPUT__AnalogSerialInput__ + i, SOURCEINPUT[i+1] );
        }
        
        VIDEOOUTPUTSOURCE = new InOutArray<StringOutput>( 32, this );
        for( uint i = 0; i < 32; i++ )
        {
            VIDEOOUTPUTSOURCE[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VIDEOOUTPUTSOURCE__AnalogSerialOutput__ + i, this );
            m_StringOutputList.Add( VIDEOOUTPUTSOURCE__AnalogSerialOutput__ + i, VIDEOOUTPUTSOURCE[i+1] );
        }
        
        
        for( uint i = 0; i < 32; i++ )
            SOURCEFORDISPLAY[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( SOURCEFORDISPLAY_OnChange_0, false ) );
            
        
        _SplusNVRAM.PopulateCustomAttributeList( true );
        
        NVRAM = _SplusNVRAM;
        
    }
    
    public override void LogosSimplSharpInitialize()
    {
        
        
    }
    
    public UserModuleClass_IESS_ROUTE_CONVERSION_NVX_V0_2 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}
    
    
    
    
    const uint SOURCEFORDISPLAY__AnalogSerialInput__ = 0;
    const uint SOURCEINPUT__AnalogSerialInput__ = 32;
    const uint VIDEOOUTPUTSOURCE__AnalogSerialOutput__ = 0;
    
    [SplusStructAttribute(-1, true, false)]
    public class SplusNVRAM : SplusStructureBase
    {
    
        public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
        
        
    }
    
    SplusNVRAM _SplusNVRAM = null;
    
    public class __CEvent__ : CEvent
    {
        public __CEvent__() {}
        public void Close() { base.Close(); }
        public int Reset() { return base.Reset() ? 1 : 0; }
        public int Set() { return base.Set() ? 1 : 0; }
        public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
    }
    public class __CMutex__ : CMutex
    {
        public __CMutex__() {}
        public void Close() { base.Close(); }
        public void ReleaseMutex() { base.ReleaseMutex(); }
        public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
    }
     public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
