#ifndef __S2_IESS_DISPLAY_ROUTING_V0_7_H__
#define __S2_IESS_DISPLAY_ROUTING_V0_7_H__




/*
* Constructor and Destructor
*/

/*
* DIGITAL_INPUT
*/
#define __S2_IESS_Display_Routing_v0_7_POWEROFF_DIG_INPUT 0

#define __S2_IESS_Display_Routing_v0_7_INPUTS_DIG_INPUT 1
#define __S2_IESS_Display_Routing_v0_7_INPUTS_ARRAY_LENGTH 16
#define __S2_IESS_Display_Routing_v0_7_OUTPUTS_DIG_INPUT 17
#define __S2_IESS_Display_Routing_v0_7_OUTPUTS_ARRAY_LENGTH 32

/*
* ANALOG_INPUT
*/
#define __S2_IESS_Display_Routing_v0_7_NUMBERDISPLAYS_ANALOG_INPUT 0
#define __S2_IESS_Display_Routing_v0_7_ROUTINGMODE_ANALOG_INPUT 1




/*
* DIGITAL_OUTPUT
*/
#define __S2_IESS_Display_Routing_v0_7_DISPLAYPOPUP_DIG_OUTPUT 0


/*
* ANALOG_OUTPUT
*/
#define __S2_IESS_Display_Routing_v0_7_LASTSELECTEDINPUT_ANALOG_OUTPUT 0


#define __S2_IESS_Display_Routing_v0_7_VIDEOSWITCH_ANALOG_OUTPUT 1
#define __S2_IESS_Display_Routing_v0_7_VIDEOSWITCH_ARRAY_LENGTH 32

/*
* Direct Socket Variables
*/




/*
* INTEGER_PARAMETER
*/
/*
* SIGNED_INTEGER_PARAMETER
*/
/*
* LONG_INTEGER_PARAMETER
*/
/*
* SIGNED_LONG_INTEGER_PARAMETER
*/
/*
* INTEGER_PARAMETER
*/
/*
* SIGNED_INTEGER_PARAMETER
*/
/*
* LONG_INTEGER_PARAMETER
*/
/*
* SIGNED_LONG_INTEGER_PARAMETER
*/
/*
* STRING_PARAMETER
*/


/*
* INTEGER
*/
CREATE_INTARRAY1D( S2_IESS_Display_Routing_v0_7, __GLBL_OUTPUTS, 32 );;


/*
* LONG_INTEGER
*/


/*
* SIGNED_INTEGER
*/


/*
* SIGNED_LONG_INTEGER
*/


/*
* STRING
*/

/*
* STRUCTURE
*/

START_GLOBAL_VAR_STRUCT( S2_IESS_Display_Routing_v0_7 )
{
   void* InstancePtr;
   struct GenericOutputString_s sGenericOutStr;
   unsigned short LastModifiedArrayIndex;

   DECLARE_IO_ARRAY( __INPUTS );
   DECLARE_IO_ARRAY( __OUTPUTS );
   DECLARE_IO_ARRAY( __VIDEOSWITCH );
   unsigned short __GLBL_LASTINPUT;
   unsigned short __GLBL_LASTOUTPUT;
   unsigned short __GLBL_COUNTDOWN;
   unsigned short __GLBL_POPUPACTIVE;
   unsigned short __GLBL_NUMBERDISPLAYS;
   unsigned short __GLBL_ROUTINGMODE;
   DECLARE_INTARRAY( S2_IESS_Display_Routing_v0_7, __GLBL_OUTPUTS );
};

START_NVRAM_VAR_STRUCT( S2_IESS_Display_Routing_v0_7 )
{
};

DEFINE_WAITEVENT( S2_IESS_Display_Routing_v0_7, __SPLS_TMPVAR__WAITLABEL_0__ );
DEFINE_WAITEVENT( S2_IESS_Display_Routing_v0_7, SOURCEHOLD );


#endif //__S2_IESS_DISPLAY_ROUTING_V0_7_H__

