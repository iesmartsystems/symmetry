#include "TypeDefs.h"
#include "Globals.h"
#include "FnctList.h"
#include "Library.h"
#include "SimplSig.h"
#include "S2_IESS_Route_Conversion_NVX_v0_1.h"

FUNCTION_MAIN( S2_IESS_Route_Conversion_NVX_v0_1 );
EVENT_HANDLER( S2_IESS_Route_Conversion_NVX_v0_1 );
DEFINE_ENTRYPOINT( S2_IESS_Route_Conversion_NVX_v0_1 );








DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Route_Conversion_NVX_v0_1, 00000 /*SourceForDisplay*/ )

    {
    /* Begin local function variable declarations */
    
    unsigned short  __LVINDEX; 
    CREATE_STRING_STRUCT( S2_IESS_Route_Conversion_NVX_v0_1, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Route_Conversion_NVX_v0_1, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Route_Conversion_NVX_v0_1, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "" );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ), 74 );
    __LVINDEX = GetLocalLastModifiedArrayIndex ( S2_IESS_Route_Conversion_NVX_v0_1 ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ), 75 );
    if ( GetInOutArrayElement( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ), &Globals->S2_IESS_Route_Conversion_NVX_v0_1.__SOURCEFORDISPLAY , __LVINDEX  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ), 76 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ) , GENERIC_STRING_OUTPUT( S2_IESS_Route_Conversion_NVX_v0_1 )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ),  GLOBAL_STRING_ARRAY( S2_IESS_Route_Conversion_NVX_v0_1, __SOURCEINPUT  )    ,  GetInOutArrayElement( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ), &Globals->S2_IESS_Route_Conversion_NVX_v0_1.__SOURCEFORDISPLAY , __LVINDEX  )  )  )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Route_Conversion_NVX_v0_1, __VIDEOOUTPUTSOURCE , __LVINDEX ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ) ); }
        
        ; 
        }
    
    else 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ), 78 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ) , GENERIC_STRING_OUTPUT( S2_IESS_Route_Conversion_NVX_v0_1 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Route_Conversion_NVX_v0_1, __VIDEOOUTPUTSOURCE , __LVINDEX ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ) ); }
        
        ; 
        }
    
    
    S2_IESS_Route_Conversion_NVX_v0_1_Exit__Event_0:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}


    
    

/********************************************************************************
  Constructor
********************************************************************************/

/********************************************************************************
  Destructor
********************************************************************************/

/********************************************************************************
  static void ProcessDigitalEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessDigitalEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessAnalogEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessAnalogEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_Route_Conversion_NVX_v0_1_SOURCEFORDISPLAY_ANALOG_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_Route_Conversion_NVX_v0_1, 00000 /*SourceForDisplay*/, 0 );
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessStringEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessStringEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_IESS_Route_Conversion_NVX_v0_1 ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketConnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketConnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_IESS_Route_Conversion_NVX_v0_1 ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketStatusEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketStatusEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Route_Conversion_NVX_v0_1 ) ); 
            break ;
        
    }
}

/********************************************************************************
  EVENT_HANDLER( S2_IESS_Route_Conversion_NVX_v0_1 )
********************************************************************************/
EVENT_HANDLER( S2_IESS_Route_Conversion_NVX_v0_1 )
    {
    SAVE_GLOBAL_POINTERS ;
    CHECK_INPUT_ARRAY ( S2_IESS_Route_Conversion_NVX_v0_1, __SOURCEFORDISPLAY ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Route_Conversion_NVX_v0_1, __SOURCEINPUT ) ;
    switch ( Signal->Type )
        {
        case e_SIGNAL_TYPE_DIGITAL :
            ProcessDigitalEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_ANALOG :
            ProcessAnalogEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STRING :
            ProcessStringEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_CONNECT :
            ProcessSocketConnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_DISCONNECT :
            ProcessSocketDisconnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_RECEIVE :
            ProcessSocketReceiveEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STATUS :
            ProcessSocketStatusEvent( Signal );
            break ;
        }
        
    RESTORE_GLOBAL_POINTERS ;
    
    }
    
/********************************************************************************
  FUNCTION_MAIN( S2_IESS_Route_Conversion_NVX_v0_1 )
********************************************************************************/
FUNCTION_MAIN( S2_IESS_Route_Conversion_NVX_v0_1 )
{
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    SET_INSTANCE_POINTER ( S2_IESS_Route_Conversion_NVX_v0_1 );
    
    
    /* End local function variable declarations */
    
    INITIALIZE_IO_ARRAY ( S2_IESS_Route_Conversion_NVX_v0_1, __SOURCEFORDISPLAY, IO_TYPE_ANALOG_INPUT, __S2_IESS_Route_Conversion_NVX_v0_1_SOURCEFORDISPLAY_ANALOG_INPUT, __S2_IESS_Route_Conversion_NVX_v0_1_SOURCEFORDISPLAY_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Route_Conversion_NVX_v0_1, __VIDEOOUTPUTSOURCE, IO_TYPE_STRING_OUTPUT, __S2_IESS_Route_Conversion_NVX_v0_1_VIDEOOUTPUTSOURCE_STRING_OUTPUT, __S2_IESS_Route_Conversion_NVX_v0_1_VIDEOOUTPUTSOURCE_ARRAY_LENGTH );
    
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Route_Conversion_NVX_v0_1, __SOURCEINPUT, e_INPUT_TYPE_STRING, __S2_IESS_Route_Conversion_NVX_v0_1_SOURCEINPUT_ARRAY_NUM_ELEMS, __S2_IESS_Route_Conversion_NVX_v0_1_SOURCEINPUT_ARRAY_NUM_CHARS, __S2_IESS_Route_Conversion_NVX_v0_1_SOURCEINPUT_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Route_Conversion_NVX_v0_1, __SOURCEINPUT );
    
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_Route_Conversion_NVX_v0_1, sGenericOutStr, e_INPUT_TYPE_NONE, GENERIC_STRING_OUTPUT_LEN );
    
    
    
    
    
    
    S2_IESS_Route_Conversion_NVX_v0_1_Exit__MAIN:/* Begin Free local function variables */
    /* End Free local function variables */
    
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }
