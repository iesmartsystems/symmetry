using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_DISPLAY_ROUTING_V0_7
{
    public class UserModuleClass_IESS_DISPLAY_ROUTING_V0_7 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput POWEROFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> INPUTS;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> OUTPUTS;
        Crestron.Logos.SplusObjects.AnalogInput NUMBERDISPLAYS;
        Crestron.Logos.SplusObjects.AnalogInput ROUTINGMODE;
        Crestron.Logos.SplusObjects.DigitalOutput DISPLAYPOPUP;
        Crestron.Logos.SplusObjects.AnalogOutput LASTSELECTEDINPUT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> VIDEOSWITCH;
        ushort GLBL_LASTINPUT = 0;
        ushort GLBL_LASTOUTPUT = 0;
        ushort [] GLBL_OUTPUTS;
        ushort GLBL_COUNTDOWN = 0;
        ushort GLBL_POPUPACTIVE = 0;
        ushort GLBL_NUMBERDISPLAYS = 0;
        ushort GLBL_ROUTINGMODE = 0;
        private void POPUP (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 83;
            DISPLAYPOPUP  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 84;
            GLBL_COUNTDOWN = (ushort) ( 5 ) ; 
            __context__.SourceCodeLine = 85;
            if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_POPUPACTIVE ))  ) ) 
                { 
                __context__.SourceCodeLine = 87;
                GLBL_POPUPACTIVE = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 88;
                while ( Functions.TestForTrue  ( ( DISPLAYPOPUP  .Value)  ) ) 
                    { 
                    __context__.SourceCodeLine = 90;
                    CreateWait ( "__SPLS_TMPVAR__WAITLABEL_1__" , 100 , __SPLS_TMPVAR__WAITLABEL_1___Callback ) ;
                    __context__.SourceCodeLine = 88;
                    } 
                
                } 
            
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_1___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            
            __context__.SourceCodeLine = 92;
            GLBL_COUNTDOWN = (ushort) ( (GLBL_COUNTDOWN - 1) ) ; 
            __context__.SourceCodeLine = 93;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GLBL_COUNTDOWN < 1 ))  ) ) 
                { 
                __context__.SourceCodeLine = 95;
                DISPLAYPOPUP  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 96;
                GLBL_POPUPACTIVE = (ushort) ( 0 ) ; 
                } 
            
            
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void ROUTEALLDISPLAYS (  SplusExecutionContext __context__ ) 
        { 
        ushort LVCOUNTER = 0;
        
        
        __context__.SourceCodeLine = 105;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)GLBL_NUMBERDISPLAYS; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 107;
            VIDEOSWITCH [ LVCOUNTER]  .Value = (ushort) ( GLBL_LASTINPUT ) ; 
            __context__.SourceCodeLine = 105;
            } 
        
        
        }
        
    object INPUTS_OnPush_0 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 116;
            GLBL_LASTINPUT = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
            __context__.SourceCodeLine = 117;
            LASTSELECTEDINPUT  .Value = (ushort) ( GLBL_LASTINPUT ) ; 
            __context__.SourceCodeLine = 119;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_ROUTINGMODE == 1))  ) ) 
                {
                __context__.SourceCodeLine = 120;
                ROUTEALLDISPLAYS (  __context__  ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 121;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_ROUTINGMODE == 3))  ) ) 
                    { 
                    __context__.SourceCodeLine = 123;
                    VIDEOSWITCH [ GLBL_LASTOUTPUT]  .Value = (ushort) ( GLBL_LASTINPUT ) ; 
                    __context__.SourceCodeLine = 124;
                    GLBL_OUTPUTS [ GLBL_LASTOUTPUT] = (ushort) ( GLBL_LASTINPUT ) ; 
                    } 
                
                }
            
            __context__.SourceCodeLine = 126;
            CreateWait ( "SOURCEHOLD" , 400 , SOURCEHOLD_Callback ) ;
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
public void SOURCEHOLD_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 128;
            if ( Functions.TestForTrue  ( ( INPUTS[ GLBL_LASTINPUT ] .Value)  ) ) 
                {
                __context__.SourceCodeLine = 129;
                ROUTEALLDISPLAYS (  __context__  ) ; 
                }
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object INPUTS_OnRelease_1 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 134;
        CancelWait ( "SOURCEHOLD" ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object NUMBERDISPLAYS_OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 138;
        GLBL_NUMBERDISPLAYS = (ushort) ( NUMBERDISPLAYS  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ROUTINGMODE_OnChange_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 142;
        GLBL_ROUTINGMODE = (ushort) ( ROUTINGMODE  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object OUTPUTS_OnPush_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 146;
        GLBL_LASTOUTPUT = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 148;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_ROUTINGMODE == 2))  ) ) 
            { 
            __context__.SourceCodeLine = 150;
            VIDEOSWITCH [ GLBL_LASTOUTPUT]  .Value = (ushort) ( GLBL_LASTINPUT ) ; 
            __context__.SourceCodeLine = 151;
            GLBL_OUTPUTS [ GLBL_LASTOUTPUT] = (ushort) ( GLBL_LASTINPUT ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POWEROFF_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 156;
        GLBL_LASTINPUT = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 157;
        LASTSELECTEDINPUT  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 159;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_ROUTINGMODE == 1))  ) ) 
            {
            __context__.SourceCodeLine = 160;
            ROUTEALLDISPLAYS (  __context__  ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 161;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_ROUTINGMODE == 3))  ) ) 
                { 
                __context__.SourceCodeLine = 163;
                VIDEOSWITCH [ GLBL_LASTOUTPUT]  .Value = (ushort) ( GLBL_LASTINPUT ) ; 
                __context__.SourceCodeLine = 164;
                GLBL_OUTPUTS [ GLBL_LASTOUTPUT] = (ushort) ( GLBL_LASTINPUT ) ; 
                } 
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 174;
        GLBL_POPUPACTIVE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 175;
        GLBL_ROUTINGMODE = (ushort) ( 2 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    SocketInfo __socketinfo__ = new SocketInfo( 1, this );
    InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
    _SplusNVRAM = new SplusNVRAM( this );
    GLBL_OUTPUTS  = new ushort[ 33 ];
    
    POWEROFF = new Crestron.Logos.SplusObjects.DigitalInput( POWEROFF__DigitalInput__, this );
    m_DigitalInputList.Add( POWEROFF__DigitalInput__, POWEROFF );
    
    INPUTS = new InOutArray<DigitalInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        INPUTS[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( INPUTS__DigitalInput__ + i, INPUTS__DigitalInput__, this );
        m_DigitalInputList.Add( INPUTS__DigitalInput__ + i, INPUTS[i+1] );
    }
    
    OUTPUTS = new InOutArray<DigitalInput>( 32, this );
    for( uint i = 0; i < 32; i++ )
    {
        OUTPUTS[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( OUTPUTS__DigitalInput__ + i, OUTPUTS__DigitalInput__, this );
        m_DigitalInputList.Add( OUTPUTS__DigitalInput__ + i, OUTPUTS[i+1] );
    }
    
    DISPLAYPOPUP = new Crestron.Logos.SplusObjects.DigitalOutput( DISPLAYPOPUP__DigitalOutput__, this );
    m_DigitalOutputList.Add( DISPLAYPOPUP__DigitalOutput__, DISPLAYPOPUP );
    
    NUMBERDISPLAYS = new Crestron.Logos.SplusObjects.AnalogInput( NUMBERDISPLAYS__AnalogSerialInput__, this );
    m_AnalogInputList.Add( NUMBERDISPLAYS__AnalogSerialInput__, NUMBERDISPLAYS );
    
    ROUTINGMODE = new Crestron.Logos.SplusObjects.AnalogInput( ROUTINGMODE__AnalogSerialInput__, this );
    m_AnalogInputList.Add( ROUTINGMODE__AnalogSerialInput__, ROUTINGMODE );
    
    LASTSELECTEDINPUT = new Crestron.Logos.SplusObjects.AnalogOutput( LASTSELECTEDINPUT__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( LASTSELECTEDINPUT__AnalogSerialOutput__, LASTSELECTEDINPUT );
    
    VIDEOSWITCH = new InOutArray<AnalogOutput>( 32, this );
    for( uint i = 0; i < 32; i++ )
    {
        VIDEOSWITCH[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( VIDEOSWITCH__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( VIDEOSWITCH__AnalogSerialOutput__ + i, VIDEOSWITCH[i+1] );
    }
    
    __SPLS_TMPVAR__WAITLABEL_1___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_1___CallbackFn );
    SOURCEHOLD_Callback = new WaitFunction( SOURCEHOLD_CallbackFn );
    
    for( uint i = 0; i < 16; i++ )
        INPUTS[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( INPUTS_OnPush_0, false ) );
        
    for( uint i = 0; i < 16; i++ )
        INPUTS[i+1].OnDigitalRelease.Add( new InputChangeHandlerWrapper( INPUTS_OnRelease_1, false ) );
        
    NUMBERDISPLAYS.OnAnalogChange.Add( new InputChangeHandlerWrapper( NUMBERDISPLAYS_OnChange_2, false ) );
    ROUTINGMODE.OnAnalogChange.Add( new InputChangeHandlerWrapper( ROUTINGMODE_OnChange_3, false ) );
    for( uint i = 0; i < 32; i++ )
        OUTPUTS[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( OUTPUTS_OnPush_4, false ) );
        
    POWEROFF.OnDigitalPush.Add( new InputChangeHandlerWrapper( POWEROFF_OnPush_5, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_DISPLAY_ROUTING_V0_7 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_1___Callback;
private WaitFunction SOURCEHOLD_Callback;


const uint POWEROFF__DigitalInput__ = 0;
const uint INPUTS__DigitalInput__ = 1;
const uint OUTPUTS__DigitalInput__ = 17;
const uint NUMBERDISPLAYS__AnalogSerialInput__ = 0;
const uint ROUTINGMODE__AnalogSerialInput__ = 1;
const uint DISPLAYPOPUP__DigitalOutput__ = 0;
const uint LASTSELECTEDINPUT__AnalogSerialOutput__ = 0;
const uint VIDEOSWITCH__AnalogSerialOutput__ = 1;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
